#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate weekly article-level and section-level image suggestions.
See https://gitlab.wikimedia.org/repos/structured-data/image-suggestions.

This DAG waits for successful section topics and section alignment runs,
see `section_topics_dag.py` and `section_alignment_image_suggestions_dag.py` respectively.
"""

from datetime import datetime, timedelta
from mergedeep import merge
from os import path

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from platform_eng.config.dag_config import artifact, cassandra_default_conf, default_args, hadoop_name_node

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.operators.spark import SparkSubmitOperator, SparkSqlOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator

props = DagProperties(
    # DAG
    alerts_email='sd-alerts@lists.wikimedia.org',
    dag_id='image_suggestions',
    start_date=datetime(2022, 5, 16),
    schedule="0 0 * * 1",  # The DAG starts on Mondays at midnight
    timeout=timedelta(days=6),  # Times out on Sundays, before the next run
    catchup=False,
    tags=["structured-data-team"],
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    monthly_snapshot="",
    # Sensors
    sensors_poke_interval=timedelta(hours=1).total_seconds(),  # Check every hour
    # Spark jobs
    hive_db='analytics_platform_eng',
    coalesce=4, # The smaller the less files per partition & the longer execution time
    conda_env=artifact('image-suggestions-0.23.0-v0.23.0.conda.tgz'),
    executor_memory='20G',
    section_alignment_work_dir='/user/analytics-platform-eng/structured-data/section-alignment-suggestions',
    section_alignment_suggestions_dir='suggestions',
    section_topics_work_dir='/user/analytics-platform-eng/structured-data/section_topics',
    article_images_dir='article_images',
    # Cassandra
    spark_cassandra_connector_uri=artifact('spark-cassandra-connector-assembly-3.2.0-WMF-1.jar'),
    hive_to_cassandra_jar_uri=artifact('refinery-job-0.2.8-shaded.jar'),
    # Setting coalesce_partitions to 6 is important
    # as it defines how many parallel loaders we use for cassandra.
    # We currently use 6 as there are 6 cassandra hosts
    coalesce_partitions=6,
)
args = merge(
    {},
    default_args,
    {
        'email': props.alerts_email,
        # Regular job settings, ~15% cluster resources
        # See https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs
        # +2 GB driver memory, needed for the Commons and Cassandra tasks
        # +5 GB driver memory, since Skein Master was OOMing with just 7G
        # +1 GB executor memory, needed for the Cassandra task
        # +7 GB executor memory, needed to reliably run the job after coalesce changes from T323614
        # The job should eat up at most 64 * 16 + 12 = 1,036 GB (plus overheads)
        'driver_cores': 2,
        'driver_memory': '12G',
        'executor_cores': 4,
        'executor_memory': props.executor_memory,
        'conf': {
            'spark.dynamicAllocation.enabled': 'true',
            'spark.dynamicAllocation.maxExecutors': '64',
            'spark.shuffle.service.enabled': 'true',
            'spark.sql.shuffle.partitions': 256,
            'spark.sql.sources.partitionOverwriteMode': 'dynamic'
        },
        # Avoid skein.exceptions.DriverError: Received message larger than max (21089612 vs. 4194304)
        'skein_app_log_collection_enabled': False,
    }
)
cassandra_conf = merge(
    {},
    {
        **cassandra_default_conf,
        'spark.yarn.maxAppAttempts': 1,
    },
    args['conf']
)


def generate_spark_to_cassandra_insert_query(cassandra_table, coalesce_num, hive_columns, hive_db, hive_table, week):
    # Generate a quoted, simple `INSERT INTO` statement to be used by the spark-sql Cassandra loading tasks
    return (
        f"INSERT INTO aqs.image_suggestions.{cassandra_table} "
        f"SELECT /*+ COALESCE({coalesce_num}) */ {hive_columns} "
        f"FROM {hive_db}.{hive_table} "
        f"WHERE snapshot='{week}'"
    )


with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
) as dag:
    # Pre-condition: wait for the latest DB table snapshots (AKA partitions)
    # NOTE We pass snapshot values via Airflow"s built-in templates,
    #      see https://airflow.apache.org/docs/apache-airflow/2.6.3/templates-ref.html
    if not props.weekly_snapshot:
        weekly = "{{ data_interval_start.format('YYYY-MM-DD') }}"
    else:
        weekly = props.weekly_snapshot
    # Set the most recent monthly snapshot given the weekly one
    # NOTE A snapshot date is the *beginning* of the snapshot interval. For instance:
    #      - 2022-05-16 covers until 2022-05-22 (at 23:59:59).
    #        May is not over, so the May monthly snapshot is not available yet.
    #        Hence return end of *April*, i.e., 2022-04
    #      - 2022-05-30 covers until 2022-06-05.
    #        May is over, so the May monthly snapshot is available.
    #        Hence return end of *May*, i.e., 2022-05
    if not props.monthly_snapshot:
        monthly = "{{ data_interval_start.end_of('week').subtract(months=1).start_of('month').format('YYYY-MM') }}"
    else:
        monthly = props.monthly_snapshot
    # The Cirrus search index snapshot is 1 day before, `YYYYMMDD` format
    cirrus_weekly = "{{ data_interval_start.subtract(days=1).format('YYYYMMDD') }}"

    # Wait for weekly tables
    weekly_snapshot_sensor = NamedHivePartitionSensor(
        partition_names=[
            f"wmf.wikidata_entity/snapshot={weekly}",
            f"wmf.wikidata_item_page_link/snapshot={weekly}",
            f"structured_data.commons_entity/snapshot={weekly}",
        ],
        task_id='wait_for_weekly_tables',
        poke_interval=props.sensors_poke_interval,
        timeout=props.timeout,
    )

    # Wait for monthly tables.
    # We create as many parallel Airflow tasks as the monthly tables.
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    monthly_mediawiki_tables = [
        "category",
        "categorylinks",
        "imagelinks",
        "page",
        "page_props",
        "pagelinks",
        "revision",
    ]
    monthly_sensors = []
    for table in monthly_mediawiki_tables:
        url_sensor = URLSensor(
            url=f'{hadoop_name_node}/wmf/data/raw/mediawiki/tables/{table}/snapshot={monthly}/_PARTITIONED',
            task_id=f'wait_for_mw_{table}',
            poke_interval=props.sensors_poke_interval,
            timeout=props.timeout,
        )
        monthly_sensors.append(url_sensor)
    # `wmf_raw.mediawiki_private_linktarget` is private and has a different HDFS path
    private_table = 'linktarget'
    monthly_sensors.append(
        URLSensor(
            url=f'{hadoop_name_node}/wmf/data/raw/mediawiki_private/tables/{private_table}/snapshot={monthly}/_PARTITIONED',
            task_id=f'wait_for_mw_private_{private_table}',
            poke_interval=props.sensors_poke_interval,
            timeout=props.timeout,
        )
    )

    # Wait for Cirrus search index weighted tags
    cirrus_index_sensor = NamedHivePartitionSensor(
        partition_names=[
            f'discovery.cirrus_index_without_content/cirrus_replica=codfw/snapshot={cirrus_weekly}',
        ],
        task_id='wait_for_cirrus_index',
        poke_interval=props.sensors_poke_interval,
        timeout=props.timeout,
    )

    # Wikidata weighted tags for the Commons search index.
    # https://phabricator.wikimedia.org/T302095
    # Usage: commonswiki_file.py HIVE_DB SNAPSHOT COALESCE
    args = [props.hive_db, weekly, props.coalesce]
    gather_commons_dataset = SparkSubmitOperator.for_virtualenv(
        task_id="commons_index",
        virtualenv_archive=props.conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/commonswiki_file.py',
        application_args=args,
        launcher='skein',
    )

    # ALIS - article-level image suggestions.
    # https://phabricator.wikimedia.org/T299789
    # Usage: cassandra.py HIVE_DB SNAPSHOT COALESCE
    args = [props.hive_db, weekly, props.coalesce]
    gather_article_level_suggestions = SparkSubmitOperator.for_virtualenv(
        task_id="article_level_suggestions",
        virtualenv_archive=props.conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/cassandra.py',
        application_args=args,
        launcher='skein',
    )

    # Check ALIS count
    check_alis_count = BashOperator(
        task_id="alis_count",
        retries=0,
        email_on_failure=True,
        bash_command=(
            f"raw=$(hive -e \"SELECT COUNT(*) FROM {props.hive_db}.image_suggestions_suggestions WHERE snapshot='{weekly}' AND section_index IS NULL\"); "
            "alis=$(echo $raw | cut -d ' ' -f2); "
            "echo \"=====> There are $alis ALIS <=====\"; "
            "if [[ $alis -eq 0 ]]; then exit 42; fi"
        ),
    )

    # Wait for data that feeds section-level image suggestions.
    # NOTE The _SUCCESS file tells us that the data has finished being written
    section_data_sensors = []

    section_alignment_work_dir = props.section_alignment_work_dir
    section_alignment_suggestions_dir = props.section_alignment_suggestions_dir
    section_alignment_suggestions_path = f'{section_alignment_work_dir}/{section_alignment_suggestions_dir}/{weekly}'
    section_alignment_suggestions_sensor = URLSensor(
        url=f'{hadoop_name_node}{section_alignment_suggestions_path}/_SUCCESS',
        task_id=f'wait_for_section_alignment_suggestions',
        poke_interval=props.sensors_poke_interval,
    )
    section_data_sensors.append(section_alignment_suggestions_sensor)

    section_topics_work_dir = props.section_topics_work_dir
    section_topics_path = f'{section_topics_work_dir}/{weekly}'
    section_topics_sensor = URLSensor(
        url=f'{hadoop_name_node}{section_topics_path}/_SUCCESS',
        task_id=f'wait_for_section_topics',
        poke_interval=props.sensors_poke_interval,
    )
    section_data_sensors.append(section_topics_sensor)

    # SLIS - section-level image suggestions.
    # Usage: section_image_suggestions.py SUGGESTIONS_DB SNAPSHOT SECTION_TOPICS SECTION_ALIGNMENT_SUGGESTIONS SECTION_IMAGES
    # --coalesce COALESCE
    article_images_dir = props.article_images_dir
    article_images_path = f'{section_alignment_work_dir}/{article_images_dir}/{weekly}'
    args = [
        props.hive_db,
        weekly,
        section_topics_path,
        section_alignment_suggestions_path,
        article_images_path,
        "--coalesce", props.coalesce,
        "--denylist", path.join(section_topics_work_dir, 'section_titles_denylist', weekly),
    ]
    gather_section_level_suggestions = SparkSubmitOperator.for_virtualenv(
        task_id="section_level_suggestions",
        virtualenv_archive=props.conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/section_image_suggestions.py',
        application_args=args,
        launcher='skein',
    )

    # Check SLIS count
    check_slis_count = BashOperator(
        task_id="slis_count",
        bash_command=(
            f"raw=$(hive -e \"SELECT COUNT(*) FROM {props.hive_db}.image_suggestions_suggestions WHERE snapshot='{weekly}' AND section_index IS NOT NULL\"); "
            "slis=$(echo $raw | cut -d ' ' -f2); "
            "echo \"=====> There are $slis SLIS <=====\"; "
            "if [[ $slis -eq 0 ]]; then exit 42; fi"
        ),
        retries=0,
        email_on_failure=True,
    )

    # Suggestion flags for wiki search indices.
    # https://phabricator.wikimedia.org/T299884
    # Usage: search_indices.py HIVE_DB SNAPSHOT COALESCE
    args = [props.hive_db, weekly, props.coalesce]
    gather_search_indices_dataset = SparkSubmitOperator.for_virtualenv(
        task_id="wiki_indices",
        virtualenv_archive=props.conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/search_indices.py',
        application_args=args,
        launcher='skein',
    )

    # Feed Cassandra with image suggestions.
    # https://phabricator.wikimedia.org/T299885
    # Usage: spark3-sql \
    #   --driver-cores 1 \
    #   --master yarn \
    #   --conf spark.sql.catalog.aqs=com.datastax.spark.connector.datasource.CassandraCatalog \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.connection.host=aqs1010-a.eqiad.wmnet:9042,aqs1011-a.eqiad.wmnet:9042,aqs1012-a.eqiad.wmnet:9042 \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.auth.username=... \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.auth.password=... \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.output.batch.size.rows=1024 \
    #   --jars /srv/deployment/analytics/refinery/artifacts/org/wikimedia/analytics/refinery/refinery-job-0.2.8-shaded.jar  \
    #   --jars spark-cassandra-connector-assembly-3.2.0-WMF-1.jar  \
    #   --conf spark.dynamicAllocation.maxExecutors=64 \
    #   --conf spark.yarn.maxAppAttempts=1 \
    #   --conf spark.executor.memoryOverhead=2048  \
    #   --executor-memory 8G \
    #   --executor-cores 2 \
    #   --driver-memory 4G \
    #   --name hive_to_cassandra_suggestions \
    #       -e "INSERT INTO aqs.suggestions SELECT ......"
    spark_cassandra_connector_uri = props.spark_cassandra_connector_uri
    hive_to_cassandra_jar_uri = props.hive_to_cassandra_jar_uri
    coalesce_partitions = props.coalesce_partitions

    # NOTE We create an Airflow task for every target Cassandra table
    #      See https://phabricator.wikimedia.org/T293808

    # `suggestions` table.
    # - Initial schema at https://phabricator.wikimedia.org/T293808
    # - section heading addition at https://phabricator.wikimedia.org/T328670
    # - section index and page QID addition at https://phabricator.wikimedia.org/T336424
    feed_suggestions_query = generate_spark_to_cassandra_insert_query(
        cassandra_table="suggestions",
        coalesce_num=coalesce_partitions,
        hive_columns=(
            "wiki,page_id,id,image,origin_wiki,confidence,"
            "found_on,kind,page_rev,section_heading,section_index,page_qid"
        ),
        hive_db=props.hive_db,
        hive_table="image_suggestions_suggestions",
        week=weekly,
    )
    feed_suggestions = SparkSqlOperator(
        task_id=f"hive_to_cassandra_suggestions",
        sql=feed_suggestions_query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein",
    )

    # `title_cache` table
    feed_title_cache_query = generate_spark_to_cassandra_insert_query(
        cassandra_table="title_cache",
        coalesce_num=coalesce_partitions,
        hive_columns="wiki,page_id,page_rev,title",
        hive_db=props.hive_db,
        hive_table="image_suggestions_title_cache",
        week=weekly,
    )
    feed_title_cache = SparkSqlOperator(
        task_id=f"hive_to_cassandra_title_cache",
        sql=feed_title_cache_query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein",
    )

    # `instanceof_cache` table
    feed_instanceof_cache_query = generate_spark_to_cassandra_insert_query(
        cassandra_table="instanceof_cache",
        coalesce_num=coalesce_partitions,
        hive_columns="wiki,page_id,page_rev,instance_of",
        hive_db=props.hive_db,
        hive_table="image_suggestions_instanceof_cache",
        week=weekly,
    )
    feed_instanceof_cache = SparkSqlOperator(
        task_id=f"hive_to_cassandra_instanceof_cache",
        sql=feed_instanceof_cache_query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein",
    )

    # Push metrics to the Prometheus gateway.
    # - Explicitly pass `conda_env`
    # - invoke Python & script from the `venv` relative path
    check_data_and_push_metrics = SimpleSkeinOperator(
        task_id="check_data_and_push_metrics",
        files={"venv": props.conda_env},
        script="venv/bin/python venv/lib/python3.10/site-packages/image_suggestions/data_check.py"
    )

    # In case of upstream failure(s), relevant sensor(s) will time out,
    # so write an empty dataset to `image_suggestions_search_index_delta`.
    write_empty_search_index_delta = SparkSqlOperator(
        task_id=f"write_empty_search_index_delta",
        trigger_rule='one_failed',
        sql=(
            f"INSERT OVERWRITE TABLE {props.hive_db}.image_suggestions_search_index_delta "
            f"partition(snapshot='{weekly}') "
            f"SELECT '' as wikiid, 0 as page_namespace, 0 as page_id, '' as tag, array() as `values` where 0=1"
        ),
        launcher="skein",
    )

    # DAG's tasks order
    [
        weekly_snapshot_sensor,
        *monthly_sensors,
        cirrus_index_sensor,
    ] >> \
    gather_commons_dataset >> \
    gather_article_level_suggestions >> \
    check_alis_count >> \
    section_data_sensors >> \
    gather_section_level_suggestions >> \
    check_slis_count >> \
    gather_search_indices_dataset >> \
    feed_suggestions >> \
    feed_title_cache >> \
    feed_instanceof_cache >> \
    check_data_and_push_metrics

    [
        weekly_snapshot_sensor,
        *monthly_sensors,
        cirrus_index_sensor,
    ] >> \
    write_empty_search_index_delta
