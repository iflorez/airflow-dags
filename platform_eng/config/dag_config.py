from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = 'hdfs://analytics-hadoop'
refinery_directory = f'{hadoop_name_node}/wmf/refinery'
hql_directory = f'{refinery_directory}/current/hql'
artifacts_directory = f'{refinery_directory}/current/artifacts'
hdfs_temp_directory = '/tmp/platform_eng'

# Emails.
alerts_email = 'platform-eng-alerts@wikimedia.org'

# default_args for this Airflow Instance.
instance_default_args = {
    'owner': 'analytics-platform-eng',
    'email': alerts_email,
    'hadoop_name_node': hadoop_name_node,
    'metastore_conn_id': 'analytics-hive',
}

# config for jobs that require read or write to the AQS Cassandra cluster.
cassandra_default_conf = dag_default_args.cassandra_default_conf

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args['queue'] = 'production'

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# syntactic sugar to use when declaring artifacts in dags.
artifact_registry = ArtifactRegistry.for_wmf_airflow_instance('platform_eng')
artifact = artifact_registry.artifact_url

# Default arguments for all operators used by this airflow instance.
default_args = dag_default_args.get({
    **instance_default_args,
    # Arguments for operators that have artifact dependencies
    'hdfs_tools_shaded_jar_path': artifact('hdfs-tools-0.0.6-shaded.jar'),
    'spark_sql_driver_jar_path': artifact('wmf-sparksqlclidriver-1.0.0.jar'),
})

wikis = [
    # Initial targets
    "arwiki", "bnwiki", "cswiki", "eswiki", "idwiki", "ptwiki", "ruwiki",
    # First scale up - https://phabricator.wikimedia.org/T364374
    "ckbwiki", "frrwiki", "hywiki", "jvwiki", "kuwiki",
    "newiki", "pawiki", "simplewiki", "skwiki", "sqwiki",
    # First scale up, backup - https://phabricator.wikimedia.org/T364374#9778351
    # Uncomment if needed
    #"cewiki", "lvwiki", "nlwiki", "thwiki",
]
