from typing import Any, Optional

from airflow.operators.email import EmailOperator

from wmf_airflow_common.util import get_first_part_in_spark_output_dir, hdfs_client


class HdfsEmailOperator(EmailOperator):
    """
    Sends an email.
    Allows to embed a file from HDFS at the end of the email body.
    """

    template_fields = EmailOperator.template_fields + ("embedded_file", "spark_output_directory")

    def __init__(
        self,
        to: str,
        subject: str,
        html_content: str,
        hadoop_name_node: str,
        embedded_file: Optional[str] = None,
        spark_output_directory: Optional[str] = None,
        **kwargs: Any,
    ):
        """
        Initializes an HdfsEmailOperator (at DAG interpretation time).
        """
        if not ((embedded_file is None) ^ (spark_output_directory is None)):
            raise ValueError(
                "One, and only one, of the 2 arguments `embedded_file` "
                "or `spark_output_directory` should be provided."
            )

        # Initialize the parent EmailOperator.
        super().__init__(to=to, subject=subject, html_content=html_content, **kwargs)

        # File will be embedded in execute method (DAG execution time).
        self.hadoop_name_node = hadoop_name_node
        self.embedded_file = embedded_file
        self.spark_output_directory = spark_output_directory

    def execute(self, context: Any) -> None:
        """
        Embeds the HDFS file to the end of the file.
        Then sends the email.

        The embedded file could come from an HDFS file path or from an HDFS directory where Spark just wrote a dataset
        with its success file. When Spark writes to a directory, it may produce multiple part files Only the first file
        is read and added to the email in this case.
        """

        # Read the file contents and add them to the email body.
        self.html_content += self.get_embedded_content().replace("\n", "<br/>")

        # Let the parent EmailOperator send the email.
        super().execute(context)

    def get_embedded_content(self) -> Any:
        # Create an HDFS file system handle.
        hdfs = hdfs_client(self.hadoop_name_node)

        file_path: Optional[str] = self.embedded_file

        if self.spark_output_directory is not None:
            from urllib.parse import urlparse

            parsed_spark_output_directory = urlparse(self.spark_output_directory)
            file_path = get_first_part_in_spark_output_dir(hdfs, parsed_spark_output_directory.path)

        if hdfs.get_file_info(file_path).size == 0:
            return "<i>--empty report file on hDFS--</i>"

        with hdfs.open_input_file(file_path) as file:
            return file.read().decode("ascii")
