"""A DAG to generate article quality scores.
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.decorators import task_group
from airflow.operators.empty import EmptyOperator

from research.config import dag_config
from research.dags import snapshot_sensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "research_article_quality"
var_props = VariableProperties(f"{dag_id}_config")

mediawiki_snapshot_template = "{{data_interval_start | to_ds_month}}"
wikidata_snapshot_template = "{{data_interval_end | start_of_current_week | to_ds}}"
mediawiki_snapshot = var_props.get("mediawiki_snapshot", mediawiki_snapshot_template)
wikidata_snapshot = var_props.get("wikidata_snapshot", wikidata_snapshot_template)

wiki_list = var_props.get_list("wiki_list", [])

# optionally use a custom conda environment
# e.g. a direct link or a file using `conda package`
if env := var_props.get("conda_environment", None):
    conda_environment = env
elif gitlab_version := var_props.get("gitlab_version", None):
    article_quality_version = gitlab_version
    article_quality_conda_env_name = f"article-quality-{article_quality_version}.conda"
    conda_environment = f"https://gitlab.wikimedia.org/api/v4/projects/211/packages/generic/article-quality/{article_quality_version}/{article_quality_conda_env_name}.tgz#venv"
else:
    conda_environment = dag_config.artifact("article_quality.tgz")


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 1, 1)),
    schedule_interval="@monthly",
    dagrun_timeout=timedelta(days=30),
    catchup=False,
    user_defined_filters=filters,
    tags=["spark", "hive", "research", "article-quality"],
    default_args={**var_props.get_merged("default_args", dag_config.default_args)},
) as dag:
    features_table = var_props.get("features_table", "article_quality_dev.features")
    scores_table = var_props.get("scores_table", "article_quality_dev.scores")

    # by default, this DAG is incremental - the article features for new month are
    # appended to an existing dataset. By setting the variable 'backfill' to true,
    # a new dataset is created/overwritten
    write_mode = "append"
    start_date_bucket = var_props.get(
        "start_date_bucket", "{{data_interval_start | to_ds_nodash}}"
    )
    end_date_bucket = var_props.get(
        "end_date_bucket",
        "{{data_interval_start | end_of_current_month | to_ds_nodash}}",
    )
    coalesce_features = 10

    if var_props.get("backfill", False):
        write_mode = "overwrite"
        # for a backfill job, set the start_date_bucket to the beginning of wikipedia time...
        start_date_bucket = var_props.get("start_date_bucket", "20000101")
        coalesce_features = var_props.get("coalesce_features", 100)

    # for production, the airflow variable has to be set to "production"
    mode = var_props.get("mode", "development")
    # development mode configuration
    dev_input_config = []
    if mode == "development":
        dev_input_config.extend(
            [
                "--dev_database_name",
                var_props.get("dev_database_name", "article_quality_dev"),
                "--dev_table_prefix",
                var_props.get("dev_table_prefix", "dev_"),
            ]
        )

    projects_arg = (
        []
        if not wiki_list
        else ["--projects", ",".join([f"{w}wiki" for w in wiki_list])]
    )

    common_params = {
        "launcher": "skein",
        "driver_memory": "4G",
        # As of 07/2022 skein memory is not configurable. Turn
        # logging off in order to avoid errors like
        # "skein.exceptions.DriverError: Received message larger than
        # max (6810095 vs. 4194304)".
        "skein_app_log_collection_enabled": False if mode == "production" else True,
        # to keep the skein containers running for development
        # 'command_postamble': '; sleep 120',
    }

    spark_conf = {
        "spark.shuffle.service.enabled": "true",
        "spark.executor.memoryOverhead": "2048",
        # 'spark.sql.sources.partitionOverwriteMode': 'dynamic',
        "spark.hadoop.hive.exec.dynamic.partition": "true",
        "spark.hadoop.hive.exec.dynamic.partition.mode": "nonstrict",
        "spark.dynamicAllocation.enabled": "true",
        # 'spark.dynamicAllocation.initialExecutors': 5,
    }

    def compute_article_features():
        if var_props.get("skip_article_features", False):
            return EmptyOperator(task_id="article_features_skipped")

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 100 if mode == "production" else 30,
            "spark.sql.shuffle.partitions": 4000 if mode == "production" else 500,
        }
        return SparkSubmitOperator.for_virtualenv(
            task_id="article_features",
            **common_params,
            executor_memory="14G" if mode == "production" else "8G",
            executor_cores=8,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive=conda_environment,
            entry_point="bin/features.py",
            application_args=[
                "--mode",
                mode,
                "--start_date",
                start_date_bucket,
                "--end_date",
                end_date_bucket,
                "--mediawiki_snapshot",
                mediawiki_snapshot,
                "--output_table",
                features_table,
                "--coalesce_features",
                coalesce_features,
                "--write_mode",
                write_mode,
            ]
            + projects_arg
            + dev_input_config,
        )

    coalesce_scores = 200

    def compute_article_scores():
        if var_props.get("skip_article_scores", False):
            return EmptyOperator(task_id="article_scores_skipped")

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 100 if mode == "production" else 30,
            "spark.sql.shuffle.partitions": 1000 if mode == "production" else 50,
        }
        return SparkSubmitOperator.for_virtualenv(
            task_id="article_scores",
            **common_params,
            executor_memory="10G" if mode == "production" else "4G",
            executor_cores=8,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive=conda_environment,
            entry_point="bin/scores.py",
            application_args=[
                "--mode",
                mode,
                "--features_table",
                features_table,
                "--mediawiki_snapshot",
                mediawiki_snapshot,
                "--wikidata_snapshot",
                wikidata_snapshot,
                "--output_table",
                scores_table,
                "--coalesce_scores",
                coalesce_scores,
            ]
            + projects_arg
            + dev_input_config,
        )

    @task_group(group_id="knowledge_gaps_sensors")
    def sensors():
        _ = [
            snapshot_sensor.wait_for_mediawiki_project_namespace_map_snapshot(
                mediawiki_snapshot
            ),
            snapshot_sensor.wait_for_mediawiki_page_snapshot(mediawiki_snapshot),
            snapshot_sensor.wait_for_mediawiki_wikitext_history_snapshot(
                mediawiki_snapshot
            ),
            snapshot_sensor.wait_for_wikidata_item_page_link_snapshot(
                wikidata_snapshot
            ),
        ]

    _ = sensors() >> compute_article_features() >> compute_article_scores()
