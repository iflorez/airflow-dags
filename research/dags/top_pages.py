"""
Experimental pipeline generating hourly top viewed pages without including the number of pageviews.
"""

import os
import tempfile
from datetime import datetime
from pathlib import Path
from typing import Any

import fsspec
from airflow import DAG
from airflow.decorators import task, task_group
from airflow.operators.email import EmailOperator
from airflow.operators.empty import EmptyOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from pydantic.dataclasses import dataclass
from workflow_utils import util

from research.config import dag_config
from research.config.args import ToppagesPipelineRun
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    # Dag configuration
    conda_env: str = dag_config.artifact("research_datasets.tgz")
    # Pipeline arguments
    # By default, the dag uses the interval of the triggered run.
    hour_to_process: datetime | None = None
    # Output path of the current run. By default temporary, as the resulting
    # csv is moved to the `publish_dir`
    output_path: Path = Path(dag_config.hdfs_temp_directory) / "top_pages"
    # Number of top pages per project/country (rarely reached due to PII filters)
    top_n: int = 100
    # Threshold for pageviews, lower counts will be filtered
    pageviews_threshold: int = 100
    # Threshold for unique fingerprints, lower counts will be filtered
    fingerprint_threshold: int = 50
    # Directory for output data
    publish_dir: Path | None = None
    # Emails to send output data for QA
    top_pages_emails: list[str] | None = None


dag_id = "top_pages"
props = DagProperties.from_variable(dag_id)
default_args = dag_config.default_args | {"do_xcom_push": True}

with DAG(
    dag_id=dag_id,
    schedule="@hourly",
    start_date=datetime(2014, 1, 1),
    user_defined_filters=filters,
    default_args=default_args,
    tags=["research", "top_pages"],
    catchup=False,
) as dag:

    @task(multiple_outputs=True)
    def extract_date_to_process(
        data_interval_start: datetime | None = None,
    ) -> dict[str, Any]:
        dt = props.hour_to_process or data_interval_start
        if not dt:
            raise Exception(
                "no hour_to_process or schedule to determine what data to process",
            )

        return {
            "date_str": dt.strftime("%Y_%m_%dT%H"),
            "hour_to_process": dt,
            "year": dt.year,
            "month": dt.month,
            "day": dt.day,
            "hour": dt.hour,
        }

    @task
    def make_args(hour_to_process: datetime) -> str:
        return ToppagesPipelineRun(
            output=props.output_path,
            hour_to_process=hour_to_process,
            top_n=props.top_n,
            pageviews_threshold=props.pageviews_threshold,
            fingerprint_threshold=props.fingerprint_threshold,
        ).json()

    date_to_process = extract_date_to_process()
    hour_to_process = date_to_process["hour_to_process"]
    date_str = date_to_process["date_str"]
    year = date_to_process["year"]
    month = date_to_process["month"]
    day = date_to_process["day"]
    hour = date_to_process["hour"]

    sensor = NamedHivePartitionSensor(
        task_id="wait_for_pageviews_actor",
        partition_names=[f"wmf.pageview_actor/year={year}/month={month}/day={day}/hour={hour}"],
        poke_interval=60,
    )

    top_pages = SparkSubmitOperator.for_virtualenv(
        task_id="top_pages",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=["toppages.pipeline.run", make_args(hour_to_process)],
        driver_memory="4G",
        executor_cores=4,
        executor_memory="12G",
        max_executors=66,
        conf={"spark.hadoop.fs.permissions.umask-mode": "022"},
    )

    top_pageviews_csv = str(props.publish_dir / f"top_pages_{date_str}.csv") if props.publish_dir else None

    def publish_top_pages():
        if top_pageviews_csv:
            return HDFSArchiveOperator(
                task_id="publish_top_pages",
                source_directory=str(props.output_path),
                expected_filename_ending="csv",
                check_done=True,
                archive_file=top_pageviews_csv,
                dag=dag,
            )
        else:
            return EmptyOperator(task_id="skip_archive_top_pages")

    @task
    def embedded_file(file_path: str) -> str:
        util.fsspec_use_new_pyarrow_api(True)
        fs = fsspec.filesystem("hdfs")
        file = fs.open(file_path)
        content = file.read()
        temp_file = Path(tempfile.gettempdir()) / file_path.split("/")[-1]
        open(temp_file, mode="wb").write(content)
        return str(temp_file)

    @task
    def delete_file(file_path: str):
        os.remove(file_path)

    @task_group(group_id="top_pages_emails")
    def email_top_pages():
        if top_pageviews_csv and props.top_pages_emails:
            attachement = embedded_file(top_pageviews_csv)
            emails = [
                EmailOperator(
                    task_id=f"top_pages_email_{i}",
                    to=email,
                    subject="Top pages",
                    html_content=(
                        "Top pages<br/>"
                        f"Date: {date_str}<br/>"
                        f"Pageviews threshold: {props.pageviews_threshold}<br/>"
                        f"Fingerprints threshold: {props.fingerprint_threshold}<br/>"
                    ),
                    files=[attachement],
                    dag=dag,
                )
                for i, email in enumerate(props.top_pages_emails)
            ]

            _ = attachement >> emails >> delete_file(attachement)

        else:
            _ = EmptyOperator(task_id="no_top_pages_emails")

    _ = date_to_process >> sensor >> top_pages >> publish_top_pages() >> email_top_pages()
