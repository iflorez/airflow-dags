import getpass
import logging
from dataclasses import field
from datetime import datetime
from pathlib import Path

import fsspec
from airflow import DAG
from airflow.decorators import task, task_group
from pydantic.dataclasses import dataclass
from workflow_utils import util

from research.config import dag_config
from research.config.args import (
    Period,
    RiskobservatoryPipelineRunhighriskthresholdcomputation,
    RiskobservatoryPipelineRunrevertriskprediction,
    RiskobservatoryPipelineRunriskstatisticscomputation,
)
from research.config.wikis import WIKIS
from research.dags.snapshot_sensor import (
    wait_for_mediawiki_history_snapshot,
    wait_for_wikidiff_snapshot,
)
from wmf_airflow_common.hooks.spark import kwargs_for_virtualenv
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    conda_env: str = dag_config.artifact("research_datasets.tgz")
    # Pipeline arguments
    snapshot: str = "{{ data_interval_start | to_ds_month }}"
    start_time: str = "{{ data_interval_start | to_ds }}"
    end_time: str = "{{ data_interval_end | to_ds }}"
    wikis: list[str] = field(default_factory=lambda: WIKIS)
    model_url: str = (
        "https://analytics.wikimedia.org/published/wmf-ml-models/revertrisk/language-agnostic/20231117132654/model.pkl"
    )
    # hive database for storing revert risk predictions
    predictions_hive_database: str | None = None
    # hive database for tables used by risk observatory dashboard
    risk_observatory_hive_database: str = "risk_observatory_dev"
    hdfs_dir: Path = Path(dag_config.hdfs_temp_directory) / "risk_observatory"
    # batch size in GB
    gb_per_batch: int = 5000
    # Spark configuration
    shuffle_partitions: int = 4096
    driver_memory: str = "4G"
    executor_cores: int = 4
    executor_memory: str = "16G"
    executor_memory_overhead: str = "4G"
    max_executors: int = 98


dag_id = "risk_observatory"
props = DagProperties.from_variable(dag_id)
default_args = dag_config.default_args | {
    "do_xcom_push": True,
}
username = getpass.getuser()


@task
def create_batches(snapshot: str, start_time: str, end_time: str, hdfs_dir: str) -> list[tuple[str, str]]:
    """
    For a given snapshot, split wikis into batches of at least gb_per_batch size.
    """

    util.fsspec_use_new_pyarrow_api(True)
    fs = fsspec.filesystem("hdfs")

    batches: list[tuple[str, str]] = []
    accumulated: list[str] = []
    accumulated_size = 0

    def create_batch(wikis: list[str]) -> tuple[str, str]:
        return (
            "riskobservatory.pipeline.runrevertriskprediction",
            RiskobservatoryPipelineRunrevertriskprediction(
                snapshot=snapshot,
                wikis=wikis,
                model_url=props.model_url,
                hdfs_dir=Path(hdfs_dir),
                hive_database=props.predictions_hive_database,
                period=Period(
                    start=datetime.fromisoformat(start_time),
                    end=datetime.fromisoformat(end_time),
                ),
            ).json(),
        )

    for wiki_db in props.wikis:
        wiki_db_path = f"{dag_config.hadoop_name_node}/wmf/data/research/wikidiff/snapshot={snapshot}/wiki_db={wiki_db}"
        try:
            files = fs.ls(
                wiki_db_path,
                detail=True,
            )
        except FileNotFoundError as e:
            logging.error(f"{wiki_db}: no wikidiff data found at {wiki_db_path}", exc_info=e)
            continue

        gb = sum([f["size"] for f in files]) / (1024**3)
        accumulated_size += gb
        accumulated.append(wiki_db)
        if accumulated_size > props.gb_per_batch:
            batches.append(create_batch(accumulated))
            accumulated.clear()
            accumulated_size = 0
    if accumulated:
        batches.append(create_batch(accumulated))
    return batches


with DAG(
    dag_id=dag_id,
    schedule="@monthly",
    start_date=datetime(2019, 1, 1),
    user_defined_filters=filters,
    default_args=default_args,
    tags=["research", "risk observatory"],
    catchup=False,
) as dag:
    # extract fields that depend on templating via airflow variable
    snapshot = props.snapshot
    start_time = props.start_time
    end_time = props.end_time
    hdfs_dir = props.hdfs_dir / f"{start_time}_{end_time}"

    @task_group(group_id="wait_for_history")
    def sensors():
        _ = [
            wait_for_mediawiki_history_snapshot(props.snapshot),
            wait_for_wikidiff_snapshot(props.snapshot),
        ]

    spark_conf = {
        "spark.sql.sources.partitionOverwriteMode": "dynamic",
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.sql.adaptive.enabled": "true",
        "spark.shuffle.io.maxRetries": 10,
        "spark.task.maxFailures": 10,
        "spark.shuffle.io.retryWait": "60s",
        "spark.network.timeout": "1200s",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }

    common_revert_risk_prediction_args = kwargs_for_virtualenv(
        task_id="batch_revert_risk_prediction",
        launcher="skein",
        sla=None,
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        packages="org.apache.spark:spark-avro_2.12:3.1.2",
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
        default_env_vars={
            "https_proxy": "http://webproxy.eqiad.wmnet:8080",
            "REQUESTS_CA_BUNDLE": "/etc/ssl/certs/ca-certificates.crt",
            "SPARK_HOME": "/usr/lib/spark3",
        },
    )

    revert_risk_prediction_batches = create_batches(snapshot, start_time, end_time, str(hdfs_dir))

    revert_risk_prediction = SparkSubmitOperator.partial(**common_revert_risk_prediction_args).expand(
        application_args=revert_risk_prediction_batches,
    )

    # the remaining jobs can be run with fewer reducers
    spark_conf = {
        "spark.sql.shuffle.partitions": "512",
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.shuffle.io.maxRetries": 10,
        "spark.task.maxFailures": 10,
    }

    highrisk_threshold_args = RiskobservatoryPipelineRunhighriskthresholdcomputation(
        hdfs_dir=hdfs_dir,
        hive_database=props.risk_observatory_hive_database,
        predictions_hive_database=props.predictions_hive_database,
        # If hive database is not specified, it would use only the predictions of this run
        # As of Jan 24, the threshold are based on the period of time for which the revert risk
        # model has been trained on.
        start_time=datetime(2021, 1, 1) if props.predictions_hive_database else None,
        end_time=datetime(2022, 1, 1) if props.predictions_hive_database else None,
    )
    # compute the high risk thresholds
    highrisk_threshold = SparkSubmitOperator.for_virtualenv(
        task_id="highrisk_threshold",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
        application_args=[
            "riskobservatory.pipeline.runhighriskthresholdcomputation",
            highrisk_threshold_args.json(),
        ],
    )

    aggregate_statistics_args = RiskobservatoryPipelineRunriskstatisticscomputation(
        snapshot=props.snapshot,
        hdfs_dir=hdfs_dir,
        hive_database=props.risk_observatory_hive_database,
    )

    # aggregate statistics and update the hive tables used by the dashboard
    aggregate_statistics = SparkSubmitOperator.for_virtualenv(
        task_id="aggregate_statistics",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
        application_args=[
            "riskobservatory.pipeline.runriskstatisticscomputation",
            aggregate_statistics_args.json(),
        ],
    )

    _ = (
        sensors()
        >> revert_risk_prediction_batches
        >> revert_risk_prediction
        >> highrisk_threshold
        >> aggregate_statistics
    )
