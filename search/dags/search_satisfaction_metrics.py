from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator
from airflow.operators.bash import BashOperator
from datetime import datetime
from search.config.dag_config import YMD_PARTITION, data_path, \
    discolytics_conda_env_tgz, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor

dag_id = 'search_satisfaction_metrics'
var_props = VariableProperties(f'{dag_id}_conf')

search_satisfaction_table = var_props.get('search_satisfaction_table', 'event.searchsatisfaction')
output_table = var_props.get('output_table', 'discovery.search_satisfaction_metrics')
table_path = f'{data_path}/metrics/search_satisfaction'

default_args = {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2024, 3, 1))
}


with DAG(
    f'{dag_id}_init',
    default_args=var_props.get_merged('default_args', default_args),
    schedule_interval='@once',
) as dag_init:
    create_webreq = HiveOperator(
        task_id='create_table',
        hql=f"""
            CREATE TABLE {output_table} (
              `access_method` STRING,
              `normalized_host` STRUCT<
                  `project_class`: STRING,
                  `project`: STRING,
                  `qualifiers`: ARRAY<STRING>,
                  `tld`: STRING,
                  `project_family`: STRING>,
              `os_family` STRING,
              `country` STRING,
              `browser_family` STRING,
              `user_edit_bucket` STRING,

              `num_fulltext_serp` BIGINT,
              `num_fulltext_serp_w_click` BIGINT,
              `num_fulltext_visit_sat` BIGINT,

              `num_sessions_w_fulltext_serp` BIGINT,
              `num_sessions_w_fulltext_serp_w_click` BIGINT,
              `num_sessions_w_fulltext_visit_sat` BIGINT,

              `num_sessions_w_fulltext_dsat` BIGINT,
              `num_sessions_w_fulltext_abandon` BIGINT,

              `num_autocomplete_serp` BIGINT,
              `num_autocomplete_submit` BIGINT,
              `num_autocomplete_success` BIGINT,
              `num_autocomplete_abandon` BIGINT,

              `num_sessions_w_autocomplete_serp` BIGINT,
              `num_sessions_w_autocomplete_submit` BIGINT,
              `num_sessions_w_autocomplete_success` BIGINT,
              `num_sessions_w_autocomplete_abandon` BIGINT)
            PARTITIONED BY (
              `year` int,
              `month` int,
              `day` int
            )
            STORED AS parquet
            LOCATION '{table_path}'
        """
    ) >> EmptyOperator(task_id='complete')

with DAG(
    f'{dag_id}_v2_alter',
    default_args=var_props.get_merged('default_args', default_args),
    schedule_interval='@once',
) as dag_v2_alter:
    create_webreq = HiveOperator(
        task_id='update_table',
        hql=f"""
            ALTER TABLE {output_table}
            ADD COLUMNS (
               `num_fulltext_serp_w_results` BIGINT,
               `num_sessions_w_fulltext_serp_w_results` BIGINT
            )
        """
    ) >> EmptyOperator(task_id='complete')

with DAG(
    f'{dag_id}_daily',
    default_args=var_props.get_merged('default_args', default_args),
    schedule_interval='@daily'
) as dag_daily:
    RangeHivePartitionSensor(
        task_id='wait_for_data',
        table_name=search_satisfaction_table,
        from_timestamp='{{ execution_date }}',
        to_timestamp='{{ execution_date.add(days=1) }}',
        granularity='@hourly'
    ) >> SparkSubmitOperator.for_virtualenv(
        task_id='search_satisfaction_metrics',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/search_satisfaction_metrics.py',
        application_args=[
            '--search-satisfaction', f'{search_satisfaction_table}/{YMD_PARTITION}',
            '--output', f'{output_table}/{YMD_PARTITION}',
        ]
    ) >> BashOperator(
        task_id='chgrp',
        bash_command=f'hdfs dfs -chgrp -R analytics-privatedata-users {table_path}/{YMD_PARTITION}',
    ) >> EmptyOperator(task_id='complete')

