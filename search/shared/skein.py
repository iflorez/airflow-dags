import os
import shlex
from typing import Mapping, Optional

import skein


class OutputFileApplicationSpecBuilder:
    def __init__(
            self,
            name: str,
            entry_point: str,
            application_args: Optional[str] = None,
            queue: str = 'default',
            memory: str = '1 GiB',
            vcores: int = 1,
            venv: Optional[str] = None,
            files: Optional[Mapping[str, str]] = None,
            output_files: Optional[Mapping[str, str]] = None,
            env: Optional[Mapping[str, str]] = None,
    ):
        self._entry_point = entry_point
        self._name = name
        self._application_args = application_args
        self._queue = queue
        self._memory = memory
        self._vcores = vcores
        self._venv = venv
        self._files = files
        self._output_files = output_files
        self._env = env

    def _build_files(self) -> Mapping[str, str]:
        files = {}
        if self._venv is not None:
            files[os.path.basename(self._venv)] = self._venv
        if self._files:
            files.update(self._files)
        return files

    def _venv_executor_path(self) -> str:
        """Path inside venv on executor"""
        assert self._venv is not None
        basename = os.path.basename(self._venv)
        if basename is None:
            raise Exception('Could not detect basename from venv: {}'.format(self._venv))
        # Per skein docs it will only decompress these extensions
        if basename.lower().endswith(('.zip', '.tgz', '.tar.gz')):
            return basename
        raise Exception('Unrecognized virtualenv extension: {}'.format(basename))

    def _build_copy_outputs_script(self) -> Optional[str]:
        """Build a bash script that will copy requested outputs to hdfs"""
        if not self._output_files:
            return None
        script = []
        for local, remote in self._output_files.items():
            script.append('hdfs dfs -put -f {} {};'.format(shlex.quote(local), shlex.quote(remote)))
        return '\n'.join(script)

    def _build_primary_script(self) -> str:
        """Build a bash script that will run our application on the executor"""
        if self._venv:
            venv_path = self._venv_executor_path()
            python = os.path.join(venv_path, 'bin/python')
            entry_point = os.path.join(venv_path, self._entry_point)
        else:
            # Note that if no venv is specified then the python script must be
            # shipped as part of the files declaration.
            python = 'python3'
            entry_point = self._entry_point

        args = [entry_point]
        if self._application_args is not None:
            args += self._application_args
        arg_str = ' '.join(shlex.quote(arg) for arg in args)

        return python + ' ' + arg_str + ';'

    def _build_script(self) -> str:
        return '\n'.join(script for script in [
            # Use errexit to prevent running copy when the primary script
            # fails. If we need anything more complex it should probably be
            # wrapped into a script that gets shipped and executed instead of
            # generated here.
            'set -o errexit;',
            self._build_primary_script(),
            self._build_copy_outputs_script()
        ] if script is not None)

    def build(self) -> dict:
        # The result is transformed back into a dict so airflow templating can
        # fill out the values as necessary.
        return skein.ApplicationSpec(
            name=self._name,
            queue=self._queue,
            master=skein.Master(
                resources=skein.Resources(
                    memory=self._memory,
                    vcores=self._vcores,
                ),
                files=self._build_files(),
                script=self._build_script(),
                env=self._env,
            )
        ).to_dict()
