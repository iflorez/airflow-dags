"""
Aggregates data from `wmf.mediawiki_history` to produce a table with one row for
each globally-new user who edits during their first 30 days after registering.
In production, the results are made available in `wmf_contributors.new_editor`.

Owned by the [Movement Insights team](https://meta.wikimedia.org/wiki/Movement_Insights),
which primarily uses it to produce the [movement metrics report](https://meta.wikimedia.org/wiki/Research_and_Decision_Science/Movement_Metrics).
"""


from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    dataset,
    movement_insights_alerts_email,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "new_editor"

props = DagProperties(
    start_date=datetime(2024, 6, 1),
    sql_file=(
        "https://gitlab.wikimedia.org/repos/movement-insights/sql/-/raw/"
        "new_editor_v1.0.0/new_editor/update_new_editor.sql"
    ),
    destination_table="wmf_contributors.new_editor",
    # The SLA time counts from the end of the data interval; we need to
    # budget time for mediawiki_history to arrive
    dag_sla=timedelta(days=5),
    alerts_email=movement_insights_alerts_email,
)


with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "uses_hql", "from_hive", "to_iceberg", "requires_wmf_mediawiki_history", "all_sites"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    sensor = dataset("hive_wmf_mediawiki_history").get_sensor_for(dag)

    etl = SparkSqlOperator(
        task_id="update_new_editor",
        sql=props.sql_file,
        query_parameters={
            "destination_table": props.destination_table,
            "month": "{{ data_interval_start | to_ds_month }}",
            "mediawiki_history_snapshot": "{{ data_interval_start | to_ds_month }}",
        },
        conf={
            # Our default HDFS umask is 027, preventing read-for-all.
            # This dataset contains public-only data, so we make it
            # readable by all.
            "spark.hadoop.fs.permissions.umask-mode": "022",
        },
    )

    sensor >> etl
