"""
Get the daily abuse filter counts without any personal
or article data and insert it into the storage table
"""

from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    dataset,
    product_analytics_alerts_email,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "cx_daily_report"

props = DagProperties(
    # dag start date
    start_date=datetime(2024, 1, 15),
    # path to query file
    cx_query_file="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/56b980143a934449d4b77e3c5ed9ba0c8ec45920/content_translation/cx_abuse_filter/cx_abuse_filter_daily.hql",
    # sources
    source_table="event.contenttranslationabusefilter",
    # destinations
    destination_table="wmf_product.cx_abuse_filter_daily",
    # various
    coalesce_partitions="1",
    # SLA and alert email
    dag_sla=timedelta(days=1),
    alerts_email=product_analytics_alerts_email,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=["daily", "uses_hql", "from_hive", "to_iceberg", "requires_event_contenttranslationabusefilter", "all_sites"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # check if partition for the week is available
    sensor = dataset("hive_event_content_translation_abuse_filter").get_sensor_for(dag)

    # generate the CX report
    compute_report = SparkSqlOperator(
        task_id="report_cx_sql",
        sql=f"{props.cx_query_file}",
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table,
            "target_day": "{{ data_interval_start.day }}",
            "target_month": "{{ data_interval_start.month }}",
            "target_year": "{{ data_interval_start.year }}",
            "coalesce_partitions": props.coalesce_partitions,
        },
    )

    sensor >> compute_report
