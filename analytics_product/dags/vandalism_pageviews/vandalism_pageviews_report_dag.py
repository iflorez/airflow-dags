"""
Get the daily abuse filter counts without any personal
or article data and insert it into the storage table
"""

from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    dataset,
    product_analytics_alerts_email,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "vandalism_pageviews_dag"

props = DagProperties(
    # dag start date
    start_date=datetime(2024, 6, 27),
    # path to query file
    query_file="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/3697ba43768123ef1d331a3cf10e6a7a7160b9bf/moderation/generate_moderation_vandal_pageviews_monthly.hql",
    table_creation_hql= "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/3697ba43768123ef1d331a3cf10e6a7a7160b9bf/moderation/create_moderation_vandal_pageviews_monthly.hql",
    base_directory= "/user/analytics-product/am_potential_vandal_pageviews_monthly",
    # destination table
    destination_table="wmf_product.moderation_vandal_pageviews_monthly",
    # source tables
    canonical_table = "canonical_data.wikis",
    source_pageviews_table = "wmf.pageview_actor",
    source_history_table = "wmf.mediawiki_history",

    # SLA and alert email
    dag_sla=timedelta(days=1),
    alerts_email=product_analytics_alerts_email,

)
lang_list = ['en', 'es', 'de', 'ja', 'fr', 'ru', 'zh', 'it', 'pt', 'fa', 'ar', 'pl', 'id', 'tr']

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "uses_hql", "from_hive", "to_hive", "requires_wmf_mediawiki_history", "all_sites"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    
    # check if partition for the month is available
    sensor = dataset("hive_wmf_mediawiki_history").get_sensor_for(dag)

    # create table
    create_table = SparkSqlOperator(
        task_id="create_am_potential_vandal_pageviews_monthly",
        sql=f"{props.table_creation_hql}",
        query_parameters={
            "base_directory": props.base_directory,
            "table_name": props.destination_table,
        },
    )

    for wiki in lang_list:
    # generate report
        etl = SparkSqlOperator(
            task_id=f"vandalism_pageviews_{wiki}_dag",
            sql=f"{props.query_file}",
            query_parameters={
                "wiki_db":f"{wiki}wiki",
                "destination_table": props.destination_table,
                "snapshot": "{{ data_interval_start | to_ds_month }}",
                "source_history_table": props.source_history_table,
                "source_pageviews_table": props.source_pageviews_table,
                "canonical_table": props.canonical_table,
                "coalesce_partitions": 64,
            },
            driver_memory="12G",
            driver_cores=2,
            executor_memory="8G",
            executor_cores=2,
            conf={
                "spark.dynamicAllocation.maxExecutors": 90,
                "spark.executor.memoryOverhead": "2G",
            },
        )

        sensor >> create_table >> etl
