from datetime import datetime, timedelta

from airflow.operators.dummy import DummyOperator

from analytics_product.config.dag_config import (
    create_easy_dag,
    product_analytics_alerts_email,
    artifact
)
from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

props = DagProperties(
    wikis=[
        "wikishared"
    ],
    start_date=datetime(2024, 10, 10, 5),
    sla=timedelta(days=1),
    tags=["monthly", "to_iceberg"],
    #When testing, you don’t want to write on top of production data, rather write to a temp location, like your home folder.
    destination_table="wmf_product.campaigns_event_registration_people_monthly",
    alerts_email=product_analytics_alerts_email,

    # job repo artifact
    #When testing, the query/artifact that you are using might not be deployed to the production cluster yet. So you might want to override the production configuration with a temporary run-time value.
    conda_env=artifact("campaigns-mariadb-jobs-0.5.0-v0.5.0.conda.tgz"),
    
    # spark config settings
    spark_driver_memory="2G",
    spark_driver_cores="4",
    mysql_mariadb_driver=artifact("mysql-connector-j-8.2.0.jar"),

    mariadb_pw_file_path="/user/analytics-product/mysql-analytics-research-client-pw.txt"
)

with create_easy_dag(
    dag_id="campaigns_registration_people_monthly",
    doc_md="Calculates the monthly metrics related to campaigns event registration.",
    schedule="@monthly",
    start_date=props.start_date,
    sla=props.sla,
    tags=props.tags,
    email=props.alerts_email,
) as dag:

    spark_conf = {
        "spark.driver.memory": props.spark_driver_memory,
        "spark.driver.cores": props.spark_driver_cores,
    }

    util.dict_add_or_append_string_value(spark_conf, "spark.jars", props.mysql_mariadb_driver, ",")

    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    for wiki_db in props.wikis:

        content_args = [
            "--wiki_db",
            wiki_db,
            "--destination_table",
            props.destination_table,
            "--mariadb_pw_file",
            props.mariadb_pw_file_path
        ]

        calculate_patrolled_recentchanges = SparkSubmitOperator.for_virtualenv(
            task_id=f"{wiki_db}_campaigns_registration_people_monthly",
            virtualenv_archive=props.conda_env,
            entry_point="bin/campaigns_event_registration_people_monthly.py",
            conf=spark_conf,
            launcher="skein",
            application_args=content_args,
        )

        start >> calculate_patrolled_recentchanges >> end
