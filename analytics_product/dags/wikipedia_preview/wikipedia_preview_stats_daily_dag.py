"""
Computes wikipedia_preview_stats data daily.

* Waits for the corresponding webrequest partitions to be available.
* Populates the destination partition using a SparkSQL query file.
"""
from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    dataset,
    product_analytics_alerts_email,
)

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator


props = DagProperties(
    start_date=datetime(2023, 5, 28),
    hql_uri=(
        "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines"
        "/-/raw/4c3e9fd235ad44b50839c2d8781f896cd73c4d0b"
        "/wikipedia_preview/generate_wikipedia_preview_stats_daily.hql"
    ),
    webrequest_table="wmf.webrequest",
    wikipedia_preview_stats_table="wmf_product.wikipedia_preview_stats",
    sla=timedelta(hours=6),
    alerts_email=product_analytics_alerts_email,
)


with create_easy_dag(
    dag_id="wikipedia_preview_stats_daily",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email
) as dag:

    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)

    compute = SparkSqlOperator(
        task_id="compute_wikipedia_preview_stats",
        sql=props.hql_uri,
        query_parameters={
            "source_table": props.webrequest_table,
            "destination_table": props.wikipedia_preview_stats_table,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}"
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 16,
            "spark.yarn.executor.memoryOverhead": 2048
        }
    )

    sensor >> compute
