#!/usr/bin/env bash

# Script to check that the conda-environment.lock.yml can really be used to produce a conda environment.
# It creates the environment using the lock file, then removes it.
# In fact, sometimes `conda env export` can produce a buggy lock file.

set -e
set -x

now="$(date +%s)"
env_name="tmp_check_env_${now}"
lock_file="conda-environment.lock.yml"

conda env create --name $env_name -f $lock_file

conda env remove --name $env_name

set +x

echo "An environment could be created form $lock_file"
