import logging
import unittest
from unittest import mock

import pendulum
import pytest
from airflow.exceptions import AirflowException
from airflow_client import client
from rest_api_test_data import (
    get_dag_run_data,
    get_task_collection_data,
    get_taskinstance_data,
)

from wmf_airflow_common.sensors.rest_helper import (
    _get_dag_runs,
    _get_groups,
    _get_task_ids,
    _get_task_instances,
)

logger = logging.getLogger(__name__)


class TestRESTExternalTaskSensor:
    def setup_method(self):
        # fake URL, won't actually be used
        self.api_config = client.Configuration(host="http://localhost/api/v1")

    def test_dag_api(self):
        with mock.patch("airflow_client.client.api.dag_api.DAGApi", autospec=True) as mock_client:
            task_collection_test_data = get_task_collection_data("knowledge_gaps")

            instance = mock_client.return_value

            # test successful run
            instance.get_tasks.return_value = task_collection_test_data[0]
            result = _get_task_ids(self.api_config, "test_dag_id")
            assert result == task_collection_test_data[1], "_get_task_ids returned incorrect task IDs"

            # make sure exceptions are handled and converted to AirflowException
            instance.get_tasks.side_effect = client.ApiException("Test exception")
            with pytest.raises(AirflowException) as e_api:
                result = _get_task_ids(self.api_config, "test_dag_id")
                assert e_api.msg.contains("Test exception"), "Expected an instance of AirflowException to be raised"

    def test_groups(self):
        test_data = [
            "group1.task1",
            "group2.task2",
            "task3",
        ]

        expected = ["group1", "group2"]

        found_groups = _get_groups(test_data)

        assert sorted(found_groups) == sorted(expected)

        test_data = [
            "task1",
            "task2",
        ]

        expected = []

        found_groups = _get_groups(test_data)

        assert sorted(found_groups) == sorted(expected)

    def test_dagrun_api(self):
        success_state = ["success"]
        fail_state = ["failed"]
        dttm_filter = [pendulum.parse("2024-05-01T00:00:00+00:00")]
        fail_dttm_filter = [pendulum.parse("2022-12-01T00:00:00+00:00")]

        with mock.patch("airflow_client.client.api.dag_run_api.DAGRunApi", autospec=True) as mock_client:
            dag_run_test_data = get_dag_run_data("knowledge_gaps")
            instance = mock_client.return_value
            instance.get_dag_runs.return_value = dag_run_test_data[0]

            # test successful run and correct dttm
            result = _get_dag_runs(self.api_config, "test_dag_id", success_state, dttm_filter)
            assert len(result) == 1, "Expected to receive exactly one DAGRun object"
            # dttm filter match is sixth in a row
            assert result[0] == dag_run_test_data[0].dag_runs[5]

            # test filtering for a failed run but correct dttm
            result = _get_dag_runs(self.api_config, "test_dag_id", fail_state, dttm_filter)
            assert len(result) == 0, "Expected to receive no DAGRun objects"

            # test filtering for a successful run but incorrect dttm
            result = _get_dag_runs(self.api_config, "test_dag_id", success_state, fail_dttm_filter)
            assert len(result) == 0, "Expected to receive no DAGRun objects"

            # make sure exceptions are handled and converted to AirflowException
            instance.get_dag_runs.side_effect = client.ApiException("Test exception")
            with pytest.raises(AirflowException) as e_api:
                result = _get_dag_runs(self.api_config, "test_dag_id", success_state, dttm_filter)
                assert e_api.msg.contains("Test exception"), "Expected an instance of AirflowException to be raised"

    def test_taskinstance_api(self):
        dag_id = "knowledge_gaps"
        success_states = ["success"]
        fail_states = ["failed"]
        dttm_filter = [pendulum.parse("2024-05-01T00:00:00+00:00")]
        fail_dttm_filter = [pendulum.parse("2022-12-01T00:00:00+00:00")]

        with mock.patch("airflow_client.client.api.dag_run_api.DAGRunApi", autospec=True) as mock_dag_api:
            dag_run_test_data = get_dag_run_data(dag_id)
            dag_api = mock_dag_api.return_value
            dag_api.get_dag_runs.return_value = dag_run_test_data[0]

            with mock.patch(
                "airflow_client.client.api.task_instance_api.TaskInstanceApi", autospec=True
            ) as mock_taskinstance_api:
                taskinstance_data = get_taskinstance_data(dag_id)
                taskinstance_api = mock_taskinstance_api.return_value
                taskinstance_api.get_task_instances.return_value = taskinstance_data[0]

                # test successful run and correct dttm
                result = _get_task_instances(
                    self.api_config,
                    dag_id,
                    taskinstance_data[1],
                    success_states,
                    dttm_filter,
                )
                assert result == taskinstance_data[0].task_instances

                # test filtering for a failed run but correct dttm
                result = _get_task_instances(
                    self.api_config,
                    dag_id,
                    taskinstance_data[1],
                    fail_states,
                    dttm_filter,
                )
                assert len(result) == 0, "Expected to receive no TaskInstance objects"

                # test filtering for a successful run but incorrect dttm
                result = _get_task_instances(
                    self.api_config,
                    dag_id,
                    taskinstance_data[1],
                    success_states,
                    fail_dttm_filter,
                )
                assert len(result) == 0, "Expected to receive no TaskInstance objects"

                # make sure exceptions are handled and converted to AirflowException
                taskinstance_api.get_task_instances.side_effect = client.ApiException("Test exception")
                with pytest.raises(AirflowException) as e_api:
                    result = _get_task_instances(
                        self.api_config,
                        dag_id,
                        taskinstance_data[1],
                        success_states,
                        dttm_filter,
                    )
                    assert e_api.msg.contains("Test exception"), "Expected an instance of AirflowException to be raised"


if __name__ == "__main__":
    unittest.main()
