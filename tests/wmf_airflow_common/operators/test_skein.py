from datetime import datetime

import skein
from airflow import DAG

from wmf_airflow_common.operators.skein import SkeinOperator


def test_skein_templates_application_spec(render_task):
    with DAG(
        dag_id="test_skein_templates_application_spec",
        schedule="@daily",
        start_date=datetime(2023, 1, 1),
    ):
        op = SkeinOperator(
            task_id="pytest",
            skein_application_spec={
                "master": {"script": 'echo "{{ ds }}"'},
            },
        )

    rendered_task = render_task(op)
    assert rendered_task.make_hook()._application_spec == skein.ApplicationSpec(
        master=skein.Master(
            script='echo "2023-01-03"',
        ),
    )
