import pytest
from airflow.exceptions import AirflowFailException

from wmf_airflow_common.operators.spark import SparkSubmitOperator


def test_fail_if_avoid_local_execution():
    with pytest.raises(AirflowFailException):
        SparkSubmitOperator(
            avoid_local_execution=True, launcher=None, master="yarn", deploy_mode="client"  # type: ignore
        )

    with pytest.raises(AirflowFailException):
        SparkSubmitOperator(
            avoid_local_execution=True,
            launcher=None,  # type: ignore
            master="local",
        )


def test_default_env_vars():
    default_env_vars = {"VAR1": "test1"}
    env_vars = {"VAR1": "not_default", "VAR2": "test2"}

    # test we can set default_env_vars
    assert (
        SparkSubmitOperator(task_id="test", default_env_vars=default_env_vars).env_vars == default_env_vars
    )  # protected-access

    # test we can override default_env_vars
    assert (
        SparkSubmitOperator(task_id="test", default_env_vars=default_env_vars, env_vars=env_vars).env_vars == env_vars
    )

    # test that the original dict has not been tampered when using merge inside SparkSubmitOperator
    assert default_env_vars == {"VAR1": "test1"}
