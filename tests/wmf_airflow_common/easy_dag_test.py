from wmf_airflow_common.easy_dag import EasyDAGFactory


def test_easy_dag_with_factory_default_args():
    dag = EasyDAGFactory(
        factory_default_args={"a": 1, "b": 2},
    ).create_easy_dag(dag_id="some_dag_id", default_args={"b": 3, "c": 4})
    assert dag.default_args == {"a": 1, "b": 3, "c": 4}


def test_easy_dag_with_factory_default_args_shortcuts():
    dag = EasyDAGFactory(
        factory_default_args={"a": 1, "b": 2},
        factory_default_args_shortcuts=["b", "c"],
    ).create_easy_dag(
        dag_id="some_dag_id",
        b=3,
        c=4,
    )
    assert dag.default_args == {"a": 1, "b": 3, "c": 4}


def test_easy_dag_with_factory_user_defined_filters():
    dag = EasyDAGFactory(
        factory_user_defined_filters={"a": 1, "b": 2},
    ).create_easy_dag(
        dag_id="some_dag_id",
        user_defined_filters={"b": 3, "c": 4},
    )
    assert dag.user_defined_filters == {"a": 1, "b": 3, "c": 4}
