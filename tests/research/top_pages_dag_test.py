import json

import pytest
from airflow.models import DagBag, TaskInstance
from airflow.utils.state import DagRunState


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath() -> list[str]:
    return ["research", "dags", "top_pages.py"]


def test_top_pages_pipeline_dag_loaded(dagbag: DagBag) -> None:
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="top_pages")
    assert dag is not None
    assert len(dag.tasks) == 6


def test_top_pages_pipeline_make_args(dagbag: DagBag) -> None:
    dag = dagbag.get_dag(dag_id="top_pages")
    assert dag

    dag_run = dag.create_dagrun(
        run_id="test",
        state=DagRunState.RUNNING,
        start_date=dag.start_date,
        execution_date=dag.start_date,
    )

    prereq_ti = TaskInstance(
        task=dag.get_task(task_id="extract_date_to_process"),
        run_id=dag_run.run_id,
    )
    prereq_ti.run(
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )

    ti = TaskInstance(
        task=dag.get_task(task_id="make_args"),
        run_id=dag_run.run_id,
    )
    ti.run(
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )

    args_str = ti.xcom_pull(ti.task_id)
    assert args_str

    args = json.loads(args_str)
    expected = {
        "output": "/tmp/research/top_pages",
        "hour_to_process": "2014-01-01T00:00:00+00:00",
        "top_n": 100,
        "pageviews_threshold": 100,
        "fingerprint_threshold": 50,
    }
    assert args == expected
