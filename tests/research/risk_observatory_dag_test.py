from unittest.mock import patch

import pytest
from airflow.models import DagBag, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState

from research.config.wikis import WIKIS


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath() -> list[str]:
    return ["research", "dags", "risk_observatory_dag.py"]


def test_risk_observatory_pipeline_dag_loaded(dagbag: DagBag) -> None:
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="risk_observatory")
    assert dag
    assert len(dag.tasks) == 6


def test_risk_observatory_pipeline_mapped_tasks(dagbag: DagBag) -> None:
    dag = dagbag.get_dag(dag_id="risk_observatory")
    assert dag

    with (
        create_session() as session,
        patch("research.dags.risk_observatory_dag.fsspec.filesystem") as mocked_fsspec,
    ):
        dag_run = dag.create_dagrun(
            run_id="test",
            session=session,
            state=DagRunState.RUNNING,
            start_date=dag.start_date,
            execution_date=dag.start_date,
        )

        # Mock filesystem's `ls` method which gets invoked inside `create_batch`
        mocked_filesystem = mocked_fsspec.return_value
        mocked_filesystem.ls.return_value = [{"size": 5001 * 1024 * 1024 * 1024}]

        # Check that `create_batch` creates batches correctly
        create_batch_task_instance = TaskInstance(
            task=dag.get_task(task_id="create_batches"),
            run_id=dag_run.run_id,
        )
        create_batch_task_instance.run(
            ignore_all_deps=True,
            ignore_task_deps=True,
            ignore_ti_state=True,
            test_mode=True,
        )

        batches = create_batch_task_instance.xcom_pull(create_batch_task_instance.task_id)
        expected = [
            [
                "riskobservatory.pipeline.runrevertriskprediction",
                f'{{"snapshot": "2019-01", "wikis": ["{wiki}"], "period": {{"start": "2019-01-01T00:00:00", "end": "2019-02-01T00:00:00"}}, "model_url": "https://analytics.wikimedia.org/published/wmf-ml-models/revertrisk/language-agnostic/20231117132654/model.pkl", "hdfs_dir": "/tmp/research/risk_observatory/2019-01-01_2019-02-01", "hive_database": null}}',
            ]
            for wiki in WIKIS
        ]
        assert batches == expected

        # Check that `revert_risk_prediction` expands into the correct number of tasks
        revert_risk_prediction_task = dag.get_task(task_id="batch_revert_risk_prediction")
        expanded_tasks, _ = revert_risk_prediction_task.expand_mapped_task(dag_run.run_id, session=session)
        assert len(expanded_tasks) == len(batches)
