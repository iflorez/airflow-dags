"""
Tests for wd_item_sitelink_segments_weekly_dag.
"""

import pytest


# This fixture defines the dag_path for the shared dagbag one.
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return [
        "wmde",
        "dags",
        "wd_item_sitelink_segments",
        "wd_item_sitelink_segments_weekly_dag.py",
    ]


def test_wd_item_sitelink_segments_weekly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wd_item_sitelink_segments_weekly")
    assert dag is not None
    assert len(dag.tasks) == 4
