import pytest
from airflow.models import DagRun, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType

from analytics.dags.refine.refine_to_hive_hourly_dag_factory import (
    SPARK_JOB_SCALE_XCOM_KEY,
)
from wmf_airflow_common.util import convert_dict_to_list


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_test", "dags", "refine", "refine_to_hive_hourly_dag.py"]


def test_refine_to_hive_hourly_analytics_test_dag_loaded(dagbag, compare_with_fixture):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag_id = "refine_to_hive_hourly_test"
    dag = dagbag.get_dag(dag_id=dag_id)
    assert dag is not None
    assert len(dag.tasks) == 11  # The task groups are not counted as tasks.

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(hours=1))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    # Test the task in charge of getting the configuration
    fetch_datasets_configurations_ti = create_and_run_task("fetch_stream_configurations", dagrun)
    configurations = fetch_datasets_configurations_ti.xcom_pull(task_ids=fetch_datasets_configurations_ti.task.task_id)
    assert len(configurations) == 3
    config = next((c for c in configurations if c["stream"] == "eventgate-analytics.test.event"), None)
    assert config == {
        "diff_hive_table": "event.eventgate_analytics_test_event",
        "hdfs_source_paths": [
            "/wmf/data/raw/event/eqiad.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17",
            "/wmf/data/raw/event/codfw.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17",
        ],
        "hive_partition_columns": {
            "datacenter": "STRING",
            "day": "LONG",
            "hour": "LONG",
            "month": "LONG",
            "year": "LONG",
        },
        "hive_partition_paths": [
            "datacenter=eqiad/year=2024/month=8/day=8/hour=17",
            "datacenter=codfw/year=2024/month=8/day=8/hour=17",
        ],
        "hive_pre_partitions": ["datacenter=eqiad", "datacenter=codfw"],
        "hive_table": "event_alt.eventgate_analytics_test_event",
        "schema_uri": "/test/event/latest",
        "spark_refine_job_scale": "medium",
        "spark_refine_job_scale_params": {
            "driver_cores": 1,
            "driver_memory": "4G",
            "executor_cores": 4,
            "executor_memory": "8G",
            "master": "yarn",
            "max_executors": 4,
        },
        "stream": "eventgate-analytics.test.event",
        "table_format": "hive",
        "table_location": "hdfs://analytics-test-hadoop/user/analytics/event_alt/eventgate_analytics_test_event",
        "transform_functions": [
            "org.wikimedia.analytics.refinery.job.refine.remove_canary_events",
            "org.wikimedia.analytics.refinery.job.refine.deduplicate",
            "org.wikimedia.analytics.refinery.job.refine.geocode_ip",
            "org.wikimedia.analytics.refinery.job.refine.parse_user_agent",
            "org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain",
            "org.wikimedia.analytics.refinery.job.refine.add_normalized_host",
            "org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",
        ],
    }

    # Test the task in charge of preparing the sensor (map_index=3 is ios.search stream)
    prepare_import_paths_urls_ti = create_and_run_task(
        "refine_hive_dataset.prepare_import_paths_urls", dagrun, map_index=2
    )
    assert prepare_import_paths_urls_ti.xcom_pull(
        task_ids=prepare_import_paths_urls_ti.task.task_id, map_indexes=2
    ) == [
        "hdfs://analytics-test-hadoop/wmf/data/raw/event/eqiad.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17/_IMPORTED",  # noqa: E501
        "hdfs://analytics-test-hadoop/wmf/data/raw/event/codfw.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17/_IMPORTED",  # noqa: E501
    ]

    # Test the task in charge of preparing the parameters for the evolve hive table job
    prepare_evolve_table_script_ti = create_and_run_task(
        "refine_hive_dataset.prepare_evolve_table_script", dagrun, map_index=2
    )
    assert prepare_evolve_table_script_ti.xcom_pull(
        task_ids=prepare_evolve_table_script_ti.task.task_id, map_indexes=2
    ) == convert_dict_to_list(
        {
            "--table": "event_alt.eventgate_analytics_test_event",
            "--schema_uri": "/test/event/latest",
            "--location": "hdfs://analytics-test-hadoop/user/analytics/event_alt/eventgate_analytics_test_event",
            "--transform_functions": "org.wikimedia.analytics.refinery.job.refine.remove_canary_events,org.wikimedia.analytics.refinery.job.refine.deduplicate,org.wikimedia.analytics.refinery.job.refine.geocode_ip,org.wikimedia.analytics.refinery.job.refine.parse_user_agent,org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain,org.wikimedia.analytics.refinery.job.refine.add_normalized_host,org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",  # noqa: E501
            "--partition_columns": "datacenter:STRING,year:LONG,month:LONG,day:LONG,hour:LONG",
            "--dry_run": "false",
        }
    )

    # Test that the rendered map_index is correct
    assert prepare_evolve_table_script_ti.rendered_map_index == "eventgate-analytics.test.event"

    # Test the task which call the evolve table job
    evolve_table = dag.get_task("refine_hive_dataset.evolve_hive_table")
    evolve_table_ti = TaskInstance(evolve_table, run_id=dagrun.run_id, map_index=2)
    evolve_table_ti.dag_run = dagrun
    context = evolve_table_ti.get_template_context()
    evolve_table_ti.render_templates(context=context)
    skein_hook = evolve_table._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="skein_operator_spec", fixture_id=f"{dag_id}.evolve_hive_table", **kwargs)

    # Test the task in charge of preparing the parameters for the spark job
    prepare_application_args_ti = create_and_run_task(
        "refine_hive_dataset.prepare_refine_task_params", dagrun, map_index=2
    )
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_application_args_ti.task.task_id, map_indexes=2
    ) == convert_dict_to_list(
        {
            "--input_paths": "/wmf/data/raw/event/eqiad.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17,/wmf/data/raw/event/codfw.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17",  # noqa: E501
            "--schema_uri": "/test/event/latest",
            "--table": "event_alt.eventgate_analytics_test_event",
            "--transform_functions": "org.wikimedia.analytics.refinery.job.refine.remove_canary_events,org.wikimedia.analytics.refinery.job.refine.deduplicate,org.wikimedia.analytics.refinery.job.refine.geocode_ip,org.wikimedia.analytics.refinery.job.refine.parse_user_agent,org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain,org.wikimedia.analytics.refinery.job.refine.add_normalized_host,org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes",  # noqa: E501
            "--partition_paths": "datacenter=eqiad/year=2024/month=8/day=8/hour=17,datacenter=codfw/year=2024/month=8/day=8/hour=17",  # noqa: E501
            "--spark_job_scale": "medium",
        }
    )
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_application_args_ti.task.task_id, map_indexes=2, key=SPARK_JOB_SCALE_XCOM_KEY
    ) == {
        "driver_cores": 1,
        "driver_memory": "4G",
        "executor_cores": 4,
        "executor_memory": "8G",
        "master": "yarn",
        "max_executors": 4,
    }

    # Test the task in charge of actually refining the dataset
    refine_hourly = dag.get_task("refine_hive_dataset.refine_to_hive_hourly")
    refine_hourly_ti = TaskInstance(refine_hourly, run_id=dagrun.run_id, map_index=2)
    refine_hourly_ti.dag_run = dagrun
    context = refine_hourly_ti.get_template_context()
    refine_hourly_ti.render_templates(context=context)
    skein_hook = refine_hourly._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="spark_skein_specs", fixture_id=f"{dag_id}.refine_to_hive_hourly", **kwargs)

    # Test prepare_legacy_partition_names
    prepare_legacy_partition_names_ti = create_and_run_task(
        "refine_hive_dataset.prepare_legacy_partition_names", dagrun, map_index=2
    )
    hive_partition_ending = "year=2024/month=8/day=8/hour=17"
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_legacy_partition_names_ti.task.task_id, map_indexes=2
    ) == [
        f"event.eventgate_analytics_test_event/datacenter=eqiad/{hive_partition_ending}",
        f"event.eventgate_analytics_test_event/datacenter=codfw/{hive_partition_ending}",
    ]

    # test prepare_diff_task_params
    prepare_diff_task_params_ti = create_and_run_task(
        "refine_hive_dataset.prepare_diff_task_params", dagrun, map_index=2
    )
    assert prepare_application_args_ti.xcom_pull(
        task_ids=prepare_diff_task_params_ti.task.task_id, map_indexes=2
    ) == convert_dict_to_list(
        {
            "--legacy_table": "event.eventgate_analytics_test_event",
            "--new_table": "event_alt.eventgate_analytics_test_event",
            "--new_table_format": "hive",
            "--year": 2024,
            "--month": 8,
            "--day": 8,
            "--hour": 17,
        }
    )

    # test diff_check
    diff_check = dag.get_task("refine_hive_dataset.diff_check")
    diff_check_ti = TaskInstance(diff_check, run_id=dagrun.run_id, map_index=2)
    diff_check_ti.dag_run = dagrun
    context = diff_check_ti.get_template_context()
    diff_check_ti.render_templates(context=context)
    skein_hook = diff_check._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="spark_skein_specs", fixture_id=f"{dag_id}.diff_check", **kwargs)


def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        with create_session() as session:
            session.add(task_instance)
            session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance
