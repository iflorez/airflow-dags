import pytest

@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "moderation", "unpatrolled_recentchanges_daily_dag.py"]

def test_unpatrolled_recentchanges_daily_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="unpatrolled_recentchanges_daily")
    assert dag is not None

    # six wikis and two start & end dummy tasks
    assert len(dag.tasks) == 6+2

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"