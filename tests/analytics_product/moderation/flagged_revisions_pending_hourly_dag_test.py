import pytest

@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "moderation", "flagged_revisions_pending_hourly_dag.py"]

def test_flagged_revisions_pending_hourly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="flagged_revisions_pending_hourly")
    assert dag is not None
    assert len(dag.tasks) == 25+2

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"