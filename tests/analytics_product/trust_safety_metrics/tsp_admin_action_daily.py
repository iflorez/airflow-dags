import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "trust_safety_metrics", "trust_safety_admin_action_daily_dag.py"]


def test_tsp_admin_action_daily_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="tsp_admin_action_daily")
    assert dag is not None
    assert len(dag.tasks) == 3

