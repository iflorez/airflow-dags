import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "content_translation", "cx_daily_report_dag.py"]


def test_cx_hourly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="cx_daily_report")
    assert dag is not None
    assert len(dag.tasks) == 2
    assert dag.tasks[0].partition_names == [
        "event.contenttranslationabusefilter"
        "/year={{ data_interval_start.year }}"
        "/month={{ data_interval_start.month }}"
        "/day={{ data_interval_start.day }}"
        f"/hour={hour}"
        for hour in range(24)
    ]

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"
