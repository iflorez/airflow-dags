from copy import deepcopy
from unittest import mock

from airflow.models import DagRun, TaskInstance
from airflow.timetables.base import DataInterval


def create_task_instance(task, mocker, execution_date):
    """
    Create a task instance for the given task.
    :param mocker: a pytest-mock mocker
    :param task: an Airflow task to instantiate
    :return: a task instance for the first run of the given task
    """
    task = deepcopy(task)
    data_interval = DataInterval.exact(execution_date)
    prev_dagrun = None
    dagrun = DagRun(dag_id=task.dag_id, run_id="pytest_render_task", data_interval=data_interval)
    ti = TaskInstance(task, run_id=dagrun.run_id)
    mocker.patch.object(ti, "get_dagrun", return_value=dagrun)
    mocker.patch.object(ti, "get_previous_dagrun", return_value=prev_dagrun)
    mocker.patch.object(TaskInstance, "execution_date", mock.PropertyMock(return_value=data_interval.end))
    context = ti.get_template_context()
    context["run_id"] = dagrun.run_id
    ti.render_templates(context=context)
    return ti
