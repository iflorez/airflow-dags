import pytest


@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "mediawiki", "mediawiki_history_metrics_monthly_dag.py"]


def test_mediawiki_history_metrics_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="mediawiki_history_metrics_monthly")
    assert dag is not None
    assert len(dag.tasks) == 2
