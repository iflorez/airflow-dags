import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "session_length", "session_length_daily_dag.py"]


def test_session_length_daily_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="session_length_daily")
    assert dag is not None
    assert len(dag.tasks) == 3
