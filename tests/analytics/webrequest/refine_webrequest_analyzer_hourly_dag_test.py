import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "webrequest", "refine_webrequest_analyzer_hourly_dag.py"]


@pytest.fixture(name="dag")
def fixture_dag(dagbag):
    assert dagbag.import_errors == {}


def test_refine_webrequest_analyzer_hourly_dag_loaded(dagbag):
    dag_id = "webrequest_analyzer"
    # Should be 4 when the alerts_recipients DagProperty
    # is non empty.
    expected_num_tasks = 2
    dag = dagbag.get_dag(dag_id=dag_id)
    assert dag is not None
    assert len(dag.tasks) == expected_num_tasks
