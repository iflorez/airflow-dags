# Test locally the Airflow metrics pipeline

Airflow >> statsd-exporter >> Prometheus >> Grafana.

## Setup

Prepare the DB backend of Airflow:
```bash
docker-compose -f docker-compose.yml --profile init up
# close at the end
```

Start the Airflow stack:
```bash
docker-compose -f docker-compose.yml --profile default up -d
```

Open Grafana at http://localhost:3000 and login with `usr:pwd`.

Import the Grafana dashboard `airflow-metrics-dashboard.json`. Wait for Variables to be loaded.

## Test

Visit the Airflow UI at http://localhost:8080/home and activate some DAGs.

Run some DAGs and check the metrics in Grafana.