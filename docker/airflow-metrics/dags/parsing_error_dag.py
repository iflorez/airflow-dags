# Test dag used as an input for the Airflow metrics docker compose example.
# The scheduler will fail to parse this DAG because of the missing function call.
# type: ignore

import pendulum
from airflow import DAG
from airflow.decorators import task

with DAG(
    dag_id="parsing_error_dag",
    schedule="@monthly",
    start_date=pendulum.datetime(2023, 1, 1, tz="UTC"),
    catchup=False,
    tags=["metrics-example"],
) as dag:
    missing_function_call  # noqa: F821 <- THIS IS THE ERROR!

    @task()
    def print_array():
        pass

    print_array()
