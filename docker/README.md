# Airflow-dag docker directory

## airflow-metrics

The directory contains a docker-compose environment to test the integration of Airflow
metrics with Prometheus and Grafana.

## CI images

The Gitlab CI images used by the airflow-dag project are built with help from files in this
directory: the Blubber files and its companion shell scripts and Makefile.

### How to upgrade those images

1. Upgrade one or multiple components of the environment:
   * The conda-environment.lock.yml file (see main README.md). Leading to the main version
     e.g. `2.7.3-py3.10-20240201` (Airflow version + Python version + timestamp)
     The last part, the timestamp, describes the whole list of python dependencies and their
     versions.
   * The base Debian image. e.g. `bullseye:20240128`.
   * The Python version. e.g. `py3.10`.
2. Then bump the versions in the whole repo (with ack + sed/manually). Make sure the ci
   images versions are bumped in `.gitlab-ci.yml`. We don't want to overwrite the current
   images.
3. Commit and push the changes to open a merge request.
4. Trigger manually the CI pipeline to build the new images. You may have to tweak the
   runner's configuration to allow the pipeline to run on unprotected branches.
   https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/settings/ci_cd#js-runners-settings
5. Check the new images in the docker-registry: 
6. https://docker-registry.wikimedia.org/repos/data-engineering/airflow-dags/tags/
6. Once the jobs have built the images and pushed them to the registry, you can test them
   on the testing part of the CI pipeline (unit tests + linting)
7. You can then merge the MR. The images you have previously computed are will be
   available for the next CI pipeline runs.
