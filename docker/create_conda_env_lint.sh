set -e
set +x
source miniconda/bin/activate
conda create --name airflow python==3.10.9
conda activate airflow
pip install --quiet .[lint]
rm -Rf miniconda/pkgs
