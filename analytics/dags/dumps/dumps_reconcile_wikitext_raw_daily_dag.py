"""
************
NOTICE: This job is work in progress. If it fails, do please ignore.
************

This job checks for mismatches between the Analytics Replicas and wmf_dumps.wikitext_raw related to missing events. If
any are found, they are recorded on wmf_dumps.wikitext_mismatch_rows to later be reconciliated, alerted, and analyzed.

More info about the pyspark job at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/consistency_check.py
"""

import functools
from datetime import datetime, timedelta
from typing import List, Set

import requests
from airflow.decorators import task, task_group

from analytics.config.dag_config import artifact, create_easy_dag, spark_3_3_2_conf
from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

props = DagProperties(
    # DAG settings
    start_date=datetime(2024, 8, 11),
    sla=timedelta(hours=6),
    conda_env=artifact("mediawiki-content-dump-0.2.0.dev0-ingest-deletes-and-moves.conda.tgz"),
    # list of URIs pointing to dblist of wikis we want to include. Will be deduplicated.
    dblist_include_urls=["https://noc.wikimedia.org/conf/dblists/open.dblist"],
    # list of URIs pointing to dblist of wikis we want to exclude. Will be deduplicated.
    dblist_exclude_urls=["https://noc.wikimedia.org/conf/dblists/private.dblist"],
    # manual list of wikis that we want to exclude
    dblist_exclude=["labtestwiki"],
    # File path to the password file that the PySpark process will attempt to read.
    # The effective user of the process needs to have access to this file.
    mariadb_password_file="/user/analytics/mysql-analytics-research-client-pw.txt",
    mariadb_jdbc_driver_path=artifact("mysql-connector-j-8.2.0.jar"),
    # source tables
    hive_wikitext_raw_table="wmf_dumps.wikitext_raw_rc2",
    # target table
    hive_wikitext_inconsistent_rows_table="wmf_dumps.wikitext_inconsistent_rows_rc1",
    # Spark job tuning
    driver_memory="8G",
    driver_cores="4",
    executor_memory="8G",
    executor_cores="2",
    max_executors="16",
    # Spark JDBC Data Source parallelization
    spark_jdbc_num_partitions="1",
    # Airflow task parallelization
    max_active_tasks=16,
)


with create_easy_dag(
    dag_id="dumps_reconcile_wikitext_raw_daily",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=[
        "daily",
        "from_iceberg",
        "to_iceberg",
        "requires_wikitext_raw",
        "uses_spark",
        "mediawiki_dumps",
    ],
    sla=props.sla,
    max_active_runs=1,
    max_active_tasks=props.max_active_tasks,
    email="xcollazo@wikimedia.org",  # overriding alert email for now.
    # it's ok if this fails, and we don't want to alert ops week folk.
) as dag:
    # no sensors. we want to run this on a daily basis.

    def fetch_dblist(url) -> Set[str]:
        response = requests.get(
            url=url,
            headers={
                "User-Agent": f"WMF-Airflow-Analytics-{dag.dag_id}/1.0.0 "
                "(https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags)"
            },
            timeout=10,
        )
        response.raise_for_status()
        content = response.text
        dblist = content.splitlines()
        dblist.pop(0)  # removes comment from first line
        return set(dblist)

    @task(do_xcom_push=True)
    def calculate_effective_dblist() -> List[str]:
        includes_lists = [fetch_dblist(url) for url in props.dblist_include_urls]
        includes_set: Set[str] = functools.reduce(lambda a, b: a | b, includes_lists, set())

        excludes_lists = [fetch_dblist(url) for url in props.dblist_exclude_urls]
        excludes_set: Set[str] = functools.reduce(lambda a, b: a | b, excludes_lists, set())
        excludes_set = excludes_set | set(props.dblist_exclude)

        effective_set = includes_set - excludes_set
        effective_ordered_list = sorted(list(effective_set))

        return effective_ordered_list

    common_spark_conf = {
        **spark_3_3_2_conf,
        "spark.dynamicAllocation.maxExecutors": props.max_executors,
    }
    util.dict_add_or_append_string_value(common_spark_conf, "spark.jars", props.mariadb_jdbc_driver_path, ",")

    @task_group()
    def reconcile(wiki_db: str):
        spark_consistency_check = SparkSubmitOperator.for_virtualenv(
            task_id="spark_consistency_check",
            virtualenv_archive=props.conda_env,
            entry_point="bin/consistency_check.py",
            driver_memory=props.driver_memory,
            driver_cores=props.driver_cores,
            executor_memory=props.executor_memory,
            executor_cores=props.executor_cores,
            sla=None,  # SLA are not supported for mapped tasks
            # Override parallelization for this DAG to run more small jobs at once
            max_active_tis_per_dag=props.max_active_tasks,
            conf=common_spark_conf,
            launcher="skein",
            application_args=[
                "--wiki_db",
                wiki_db,
                "--target_table",
                props.hive_wikitext_raw_table,
                "--results_table",
                props.hive_wikitext_inconsistent_rows_table,
                # the time window is the frequency of this DAG, which AOTW, is 1 day
                # we shift it back ~2 hours to leave time for the hourly intake of wikitext_raw
                "--min_timestamp",
                "{{ data_interval_start | subtract_hours(2) | to_dt() }}",
                "--max_timestamp",
                "{{ data_interval_end | subtract_hours(2) | to_dt() }}",
                "--spark_jdbc_num_partitions",
                props.spark_jdbc_num_partitions,
                "--computation_dt",
                "{{ data_interval_end | to_dt() }}",
                "--mariadb_password_file",
                props.mariadb_password_file,
            ],
            use_virtualenv_spark=True,
            default_env_vars={
                "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
                "SPARK_CONF_DIR": "/etc/spark3/conf",
            },
        )

        # TODO: Other steps like DELETE reconciliation and EventBus Late events API calls go here.

        spark_consistency_check

    dblist = calculate_effective_dblist()
    reconcile.expand(wiki_db=dblist)
