"""
# Browser All Sites, Mobile and Desktop Reports

This job computes a group of weekly browser traffic reports
for all sites, desktop and mobile sourced from browser_general table.

The queries for each report is ran by spark and the result is saved in
a temporary location.
The archive operator is used to move the report from the temporary
location to a dedicated directory which is rsynced to an analytics server.

# Note: Every week, the report from the very beginning until the current
date is calculated. This is because there should be a single file per
report and spark does not append file.
This process is inexpensive for us because the browser_general table is
a small table

# The week is considered to start on Sunday for convenience,
so that the weekly results are already available on Monday considering the
one day sla.


* located in /wmf/data/wmf/browser/general
"""

from datetime import datetime, timedelta

from airflow.models.baseoperator import chain
from airflow.sensors.external_task import ExternalTaskSensor

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.sensors.helper import get_daily_execution_dates_fn

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # dag start date
    start_date=datetime(2024, 4, 7),
    # Jar containing Data pivot application.
    pivot_application=artifact("refinery-job-0.2.47-shaded.jar"),
    # input table
    browser_source_table="wmf_traffic.browser_general",
    # HQL queries base path
    base_hql_path=f"{hql_directory}/reports/browser_metrics",
    # output base path
    base_output_path=f"{hadoop_name_node}/wmf/data/published/datasets/periodic/reports/metrics/browser",
    # base temporary output path where output of query will be saved before
    # moving to final destination
    base_temp_directory=f"{hadoop_name_node}/wmf/tmp/analytics/reports/browser",
    # SLA and alert email
    dag_sla=timedelta(days=1),
    alerts_email=alerts_email,
)

# all_sites HQL queries file names and pivot status.
# If true then pivot report
# If false do not pivot
query_name_and_if_pivot = {
    "all_sites_by_browser_family_and_major": False,
    "all_sites_by_browser_family_and_major_percent": False,
    "all_sites_by_browser_family_percent": True,
    "all_sites_by_os_and_browser": False,
    "all_sites_by_os_and_browser_percent": False,
    "all_sites_by_os_family_and_major": False,
    "all_sites_by_os_family_and_major_percent": False,
    "all_sites_by_os_family_percent": True,
    "mobile_site_by_browser_family_and_major": False,
    "mobile_site_by_browser_family_and_major_percent": False,
    "mobile_site_by_browser_family_percent": True,
    "mobile_site_by_os_family_and_major": False,
    "mobile_site_by_os_family_and_major_percent": False,
    "mobile_site_by_os_family_percent": True,
    "desktop_site_by_os_family_and_major": False,
    "desktop_site_by_os_family_and_major_percent": False,
    "desktop_site_by_os_family_percent": True,
    "desktop_site_by_browser_family_and_major_percent": False,
    "desktop_site_by_browser_family_percent": True,
    "desktop_site_by_browser_family_and_major": False,
}

with create_easy_dag(
    dag_id="browser_metrics_weekly",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@weekly",
    tags=["uses_archiver", "to_hdfs", "uses_hql", "uses_spark", "requires_wmf_traffic_browser_general"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # check if partition for the week is available
    # iceberg sensors are not ready yet so we sense the
    # task populating the browser_general iceberg table.
    # sensor = dataset("iceberg_wmf_traffic_browser_general").get_sensor_for(dag)
    sensor = ExternalTaskSensor(
        task_id="wait_for_browser_general_iceberg_table",
        external_dag_id="browser_general_daily",
        external_task_id="summarize_traffic_stats_iceberg",
        execution_date_fn=get_daily_execution_dates_fn(num_days=7),
    )

    # generate reports for all broswer queries
    for query, pivot in query_name_and_if_pivot.items():
        operators = [sensor]

        # Path where computed report is saved temporarily
        temporary_directory = props.base_temp_directory + "/" + query + "_{{data_interval_end|to_ds}}"

        # Final location of report which will be synced with analytics server.
        output_directory = props.base_output_path + "/" + query + ".tsv"

        # generate reports for all broswer desktop
        # queries from fixed start date to current date.
        compute_report = SparkSqlOperator(
            task_id=f"compute_{query}",
            sql=props.base_hql_path + "/" + query + ".hql",
            query_parameters={
                "source_table": props.browser_source_table,
                "destination_directory": temporary_directory,
                "start_date": "2015-06-07",
                "end_date": "{{data_interval_end|to_ds}}",
            },
        )

        operators.append(compute_report)

        # Perform pivot task for reports that need to be pivoted
        if pivot:
            pivoted_directory = props.base_temp_directory + "/" + query + "_{{data_interval_end|to_ds}}_pivoted"

            pivot_report = SparkSubmitOperator(
                task_id=f"perform_pivot_on_{query}",
                application=props.pivot_application,
                java_class="org.wikimedia.analytics.refinery.job.DataPivoter",
                application_args={
                    "--source_directory": temporary_directory,
                    "--destination_directory": pivoted_directory,
                },
            )

            operators.append(pivot_report)

            hdfs_cleaner_pivot = SimpleSkeinOperator(
                task_id=f"remove_unpivoted_directory_for_{query}",
                script=f"hdfs dfs -rm -r {temporary_directory}",
            )

            operators.append(hdfs_cleaner_pivot)

            temporary_directory = pivoted_directory

        # archive file and move to output path.
        move_report_to_archive = HDFSArchiveOperator(
            task_id=f"move_{query}_report",
            source_directory=temporary_directory,
            archive_file=output_directory,
            expected_filename_ending=".csv",  # gets written by spark with .csv extension, but it's really a .tsv
            check_done=True,
        )

        operators.append(move_report_to_archive)

        # chain the collected operators up
        chain(*operators)
