"""
# Browser usage

This job computes weekly browser usage stats from pageview_hourly.
The results serve as an intermediate table for various traffic reports,
i.e.: mobile web browser breakdown, desktop os breakdown, or
desktop+mobile web os+browser breakdown, etc.

#Note: The resulting output is;
* partitioned by year, month, day
* located in /wmf/data/wmf/browser/general
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # The least number of views per group we want to output to protect privacy.
    privacy_threshold=250,
    # The least number of views per group we want to output to keep the output size useful.
    output_threshold=15000,
    dag_start_date=datetime(2015, 6, 1),
    # SLA and alert email
    dag_sla=timedelta(hours=6),
    alerts_email=alerts_email,
    # HQL
    hql_iceberg=f"{hql_directory}/browser/general/browser_general_iceberg.hql",
    # source tables
    projectview_source="wmf.projectview_hourly",
    pageview_source="wmf.pageview_hourly",
    # destination table
    browser_general_dest_iceberg="wmf_traffic.browser_general",
)

# Instantiate DAG
with create_easy_dag(
    dag_id="browser_general_daily",
    description="write_traffic_stats_data_to_hive",
    doc_md=__doc__,
    start_date=props.dag_start_date,
    schedule="@daily",
    tags=["daily", "from_hive", "to_iceberg", "uses_hql", "requires_wmf_pageview", "requires_wmf_projectview"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # Sensors to check that all hive partitions are present
    projectview_sensor = dataset("hive_wmf_projectview_hourly").get_sensor_for(dag)

    pageview_sensor = dataset("hive_wmf_pageview_hourly").get_sensor_for(dag)

    # ETL Task to execute hql and write to destination
    compute_browser_general_iceberg = SparkSqlOperator(
        task_id="summarize_traffic_stats_iceberg",
        sql=props.hql_iceberg,
        query_parameters={
            "projectview_source": props.projectview_source,
            "pageview_source": props.pageview_source,
            "destination_table": props.browser_general_dest_iceberg,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "privacy_threshold": props.privacy_threshold,
            "output_threshold": props.output_threshold,
            "os_family_unknown": "Redacted",
            "os_major_unknown": "Redacted",
            "browser_family_unknown": "Redacted",
            "browser_major_unknown": "Redacted",
            "coalesce_partitions": 1,
        },
    )

    [projectview_sensor, pageview_sensor] >> compute_browser_general_iceberg
