"""
In this DAG, we launch monthly maintenance for Iceberg tables.

wmf_airflow_common.dataset.IcebergDataset.DEFAULT_MAINTENANCE defines the default Spark CALL procedures that will
be scheduled for all Iceberg tables. These defaults can be overridden via config file analytics/config/datasets.yaml.

Example configuration from analytics/config/datasets.yaml:
```
...
iceberg_wmf_dumps_wikitext_raw:
  datastore: iceberg
  table_name: wmf_dumps.wikitext_raw_rc2

iceberg_wmf_dumps_wikitext_inconsistent_rows:
  datastore: iceberg
  table_name: wmf_dumps.wikitext_inconsistent_rows_rc1
  maintenance:
    data_delete:
      enabled: True
      where_clause: "computation_dt <= TIMESTAMP '{{ data_interval_end | subtract_days(30) | to_dt() }}'"
...
```

The fact that 'iceberg_wmf_dumps_wikitext_raw' is defined as an iceberg dataset above resolves into picking up the
following default Spark procedure CALLs:

 * `spark_catalog.system.remove_orphan_files()`
    -> This removes any data files that are not tied to any Iceberg snapshot.
       These orphan files can occur when writes fail and leave files behind.
 * `spark_catalog.system.expire_snapshots()`
    -> Iceberg keeps an immutable copy of the state of the table for each commit, so that we can 'go back in time'
       This procedure removes older snapshots and thus older files that are not needed anymore.
 * `spark_catalog.system.rewrite_manifests()`
    -> Over time, Iceberg tables may accumulate many manifest files that can slow down query planning.
       This procedure rewrites them so that reading is more efficient.

Data deletion is also possible, although disabled by default. The example 'iceberg_wmf_dumps_wikitext_inconsistent_rows'
above shows how to enable data deletion and how to define the WHERE clause to utilize. You can include jinja templates.
"""

from datetime import datetime, timedelta
from typing import cast

from airflow.models.baseoperator import chain
from airflow.utils.task_group import TaskGroup

from analytics.config.dag_config import create_easy_dag, dataset_registry
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.dataset import IcebergDataset

props = DagProperties(
    start_date=datetime(2024, 8, 15),
    dag_sla=timedelta(days=5),
)

tags = [
    "table_maintenance",
    "uses_hql",
    "monthly",
]

with create_easy_dag(
    dag_id="table_maintenance_iceberg_monthly",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=tags,
    sla=props.dag_sla,
) as dag:
    common_spark_conf = {
        "spark.sql.autoBroadcastJoinThreshold": "-1",  # avoid OOMs. See T338065#9192096.
    }

    idatasets = (
        cast(IcebergDataset, dataset)
        for dataset in dataset_registry.get_all_datasets()
        if isinstance(dataset, IcebergDataset) and dataset.maintenance["schedule"] == "@monthly"
    )

    for idataset in idatasets:
        task_group_id = idataset.table_name.replace(".", "__")
        with TaskGroup(group_id=task_group_id):
            maintenance_tasks = idataset.get_maintenance_tasks(dag, common_spark_conf)
            chain(*maintenance_tasks)
