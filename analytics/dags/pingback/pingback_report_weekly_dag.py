"""
Airflow DAG conversion of the pingback report
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hdfs_temp_directory,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "pingback_report_weekly_v2"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # dag start date
    start_date=datetime(2023, 9, 1),
    # base path to query files
    pingback_hql_basepath=f"{hql_directory}/reports/pingback",
    # sources
    source_table="event_sanitized.mediawikipingback",
    # temp directory for query results
    base_tmp_directory=f"{hadoop_name_node}{hdfs_temp_directory}/reports/pingback/tmp",
    # moving to final destination
    agg_results_path=f"{hadoop_name_node}/wmf/data/published/datasets/periodic/reports/metrics/pingback",
    # SLA and alert email
    dag_sla=timedelta(days=7),
    alerts_email=alerts_email,
)

# HQL files + explode_by determination
queries = {
    "arch": None,
    "count": None,
    "database": None,
    "machine": None,
    "memoryLimit": None,
    "os": None,
    "php": None,
    "php_drilldown": [
        (
            "MediaWiki_Version",
            [
                # Please note that adding new MediaWiki versions will generate
                # new TSV files that need to be added to the Dashiki dashboard
                # https://pingback.wmflabs.org/#php-version
                #
                # You can configure the Dashiki dashboard at this URL:
                # https://meta.wikimedia.org/wiki/Config:Dashiki:Pingback
                "1.28",
                "1.29",
                "1.30",
                "1.31",
                "1.32",
                "1.33",
                "1.34",
                "1.35",
                "1.36",
                "1.37",
                "1.38",
                "1.39",
                "1.40",
                "1.41",
                "1.42",
                "1.43",
                "1.44",
            ],
        ),
    ],
    "serverSoftware": None,
    "version": None,
    "version_simple": None,
}

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@weekly",
    tags=[
        "weekly",
        "uses_hql",
        "from_hive",
        "uses_archiver",
        "requires_event_sanitized_mediawikipingback",
        "all_sites",
    ],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # check if partition for the week is available
    sensor = dataset("hive_event_sanitized_mediawikipingback").get_sensor_for(dag)

    # generate reports for all pingback queries
    for query, explode_by in queries.items():
        # collect SparkSqlOperator objects in this list
        operator_params = []

        if explode_by:
            for explode_dim, dim_values in explode_by:
                for dim_value in dim_values:
                    task_id = f"compute_{query}_{explode_dim}_{dim_value}"

                    # Path where the computed report is saved temporarily
                    tmp_dir = (
                        props.base_tmp_directory
                        + "/"
                        + query
                        + f"_{explode_dim}_{dim_value}"
                        + "_{{data_interval_start|to_ds}}"
                    )

                    op_params_dict = {
                        "query": query,
                        "task_id": task_id,
                        "sql": props.pingback_hql_basepath + "/" + query + ".hql",
                        "tmp_dir": tmp_dir,
                        "query_parameters": {
                            "source_table": props.source_table,
                            "agg_results_path": props.agg_results_path + "/" + query + f"/{dim_value}.tsv",
                            "destination_dir": tmp_dir,
                            "explode_by": dim_value,
                            "start_date": "{{data_interval_start|to_ds}}",
                            "end_date": "{{data_interval_end|to_ds}}",
                        },
                    }

                    operator_params.append(op_params_dict)
        else:
            # Path where the computed report is saved temporarily
            tmp_dir = props.base_tmp_directory + "/" + query + "_{{data_interval_start|to_ds}}"

            op_params_dict = {
                "query": query,
                "task_id": f"compute_{query}",
                "sql": props.pingback_hql_basepath + "/" + query + ".hql",
                "tmp_dir": tmp_dir,
                "query_parameters": {
                    "source_table": props.source_table,
                    "agg_results_path": props.agg_results_path + "/" + query + ".tsv",
                    "destination_dir": tmp_dir,
                    "start_date": "{{data_interval_start|to_ds}}",
                    "end_date": "{{data_interval_end|to_ds}}",
                },
            }

            operator_params.append(op_params_dict)

        for op_params in operator_params:
            tmp_dir = op_params["tmp_dir"]
            task_id = op_params["task_id"]
            sql = op_params["sql"]
            query_params = op_params["query_parameters"]
            agg_results_path = query_params["agg_results_path"]

            # generate reports for all pingback all_sites queries from start date
            compute_report = SparkSqlOperator(
                task_id=task_id,
                sql=sql,
                query_parameters=query_params,
            )

            # archive file and move to final output path.
            move_report_to_archive = HDFSArchiveOperator(
                task_id=f"move_{task_id}",
                source_directory=tmp_dir,
                archive_file=agg_results_path,
                expected_filename_ending=".csv",  # gets written by spark with .csv extension, but it's really a .tsv
                check_done=True,
            )

            sensor >> compute_report >> move_report_to_archive
