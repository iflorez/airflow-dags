"""
* This job runs the hql that aggregates virtualpageview
* from the event.virtualpageview table.

Note
* This job waits for the event.virtualpageview partition data for the hour,
* to be available before running the aggregate hql query.
* output of the hql is then put in the wmf.virtualpageview_hourly table,
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    artifact,
    dataset,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "virtualpageview_hourly"
var_props = VariableProperties(f"{dag_id}_config")
year = "{{data_interval_start.year}}"
month = "{{data_interval_start.month}}"
day = "{{data_interval_start.day}}"
hour = "{{data_interval_start.hour}}"


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 3, 27)),
    schedule="@hourly",
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=6),
        },
    ),
    user_defined_filters=filters,
    tags=["hourly", "from_hive", "to_hive", "uses_hql", "requires_event_virtualpageview"],
) as dag:
    sensor = dataset("hive_event_virtualpageview").get_sensor_for(dag)

    etl1 = SparkSqlOperator(
        task_id="load_virtualpageview_hourly",
        sql=var_props.get("hql_path", f"{hql_directory}/virtualpageview/virtualpageview_hourly.hql"),
        query_parameters={
            "source_table": var_props.get("source_table", "event.virtualpageview"),
            "destination_table": var_props.get("destination_table", "wmf.virtualpageview_hourly"),
            "refinery_hive_jar": var_props.get("refinery_hive_jar", artifact("refinery-hive-0.2.1-shaded.jar")),
            "record_version": var_props.get("record_version", "0.0.1"),
            "allowlist_table": var_props.get("allowlist_table", "wmf.pageview_allowlist"),
            "coalesce_partitions": var_props.get("coalesce_partitions", 2),
            "year": year,
            "month": month,
            "day": day,
            "hour": hour,
        },
    )

    # this is needed to trigger the druid loading job still in Oozie
    # TODO: Remove this step when druid jobs have been migrated to airflow
    success = URLTouchOperator(
        task_id="write_success_file",
        url=(
            hadoop_name_node
            + var_props.get(
                "destination_path",
                f"/wmf/data/wmf/virtualpageview/hourly/year={year}/month={month}/day={day}/hour={hour}/_SUCCESS",
            )
        ),
    )

    sensor >> etl1 >> success
