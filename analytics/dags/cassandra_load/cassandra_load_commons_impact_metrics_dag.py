"""
Loads the Commons Impact Metrics serving layer data from Iceberg tables into Cassandra

Serving layer design at:
https://docs.google.com/document/d/1sWPJzO9J6nwfhzJAwrbWZQBcvwiT8UhBBfn2kFW3pNQ/edit#heading=h.trp207kw0b93
"""

from datetime import datetime, timedelta

from airflow.models.baseoperator import chain
from airflow.sensors.external_task import ExternalTaskSensor

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

props = DagProperties(
    # define the configuration for the casssandra jobs
    dataset_config={
        "commons_category_metrics_snapshot": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_category_metrics_snapshot.hql",
            "source_table": "wmf_contributors.commons_category_metrics_snapshot",
            "destination_table": "aqs.commons.category_metrics_snapshot",
        },
        "commons_edits_per_category_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_edits_per_category_monthly.hql",
            "source_table": "wmf_contributors.commons_edits",
            "destination_table": "aqs.commons.edits_per_category_monthly",
        },
        "commons_edits_per_user_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_edits_per_user_monthly.hql",
            "source_table": "wmf_contributors.commons_edits",
            "destination_table": "aqs.commons.edits_per_user_monthly",
        },
        "commons_media_file_metrics_snapshot": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_media_file_metrics_snapshot.hql",
            "source_table": "wmf_contributors.commons_media_file_metrics_snapshot",
            "destination_table": "aqs.commons.media_file_metrics_snapshot",
        },
        "commons_pageviews_per_category_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_pageviews_per_category_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_category_monthly",
            "destination_table": "aqs.commons.pageviews_per_category_monthly",
        },
        "commons_pageviews_per_media_file_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/"
            f"load_cassandra_commons_pageviews_per_media_file_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_media_file_monthly",
            "destination_table": "aqs.commons.pageviews_per_media_file_monthly",
        },
        "commons_top_edited_categories_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_top_edited_categories_monthly.hql",
            "source_table": "wmf_contributors.commons_edits",
            "destination_table": "aqs.commons.top_edited_categories_monthly",
        },
        "commons_top_editors_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_top_editors_monthly.hql",
            "source_table": "wmf_contributors.commons_edits",
            "destination_table": "aqs.commons.top_editors_monthly",
        },
        "commons_top_pages_per_category_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_top_pages_per_category_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_category_monthly",
            "destination_table": "aqs.commons.top_pages_per_category_monthly",
        },
        "commons_top_pages_per_media_file_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/"
            f"load_cassandra_commons_top_pages_per_media_file_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_media_file_monthly",
            "destination_table": "aqs.commons.top_pages_per_media_file_monthly",
        },
        "commons_top_viewed_categories_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_top_viewed_categories_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_category_monthly",
            "destination_table": "aqs.commons.top_viewed_categories_monthly",
        },
        "commons_top_viewed_media_files_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_top_viewed_media_files_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_media_file_monthly",
            "destination_table": "aqs.commons.top_viewed_media_files_monthly",
        },
        "commons_top_wikis_per_category_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/load_cassandra_commons_top_wikis_per_category_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_category_monthly",
            "destination_table": "aqs.commons.top_wikis_per_category_monthly",
        },
        "commons_top_wikis_per_media_file_monthly": {
            "hql_file": f"{hql_directory}/cassandra/monthly/"
            f"load_cassandra_commons_top_wikis_per_media_file_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_media_file_monthly",
            "destination_table": "aqs.commons.top_wikis_per_media_file_monthly",
        },
    },
    # DAG start date
    start_date=datetime(2023, 10, 1),
    # SLAs and alerts email.
    dag_sla=timedelta(days=5),
    alerts_email=alerts_email,
)

tags = [
    "from_iceberg",
    "to_cassandra",
    "uses_hql",
    "monthly",
    "requires_wmf_contributors_commons_category_metrics_snapshot",
    "requires_wmf_contributors_commons_media_file_metrics_snapshot",
    "requires_wmf_contributors_commons_pageviews_by_category",
    "requires_wmf_contributors_commons_pageviews_by_media_file",
    "requires_wmf_contributors_commons_edits",
]

with create_easy_cassandra_loading_dag(
    dag_id="load_commons_impact_metrics",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=tags,
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    sensor = ExternalTaskSensor(
        task_id="wait_for_source_data",
        external_dag_id="commons_impact_metrics_monthly",
    )

    etls = []
    for dataset_name, dataset_properties in props.dataset_config.items():
        etls.append(
            SparkSqlOperator(
                task_id=f"load_cassandra_{dataset_name}",
                sql=dataset_properties["hql_file"],
                query_parameters={
                    "source_table": dataset_properties["source_table"],
                    "destination_table": dataset_properties["destination_table"],
                    "coalesce_partitions": 6,
                    "year_month": "{{data_interval_start | to_ds_month}}",
                },
            )
        )

    chain(sensor, *etls)
